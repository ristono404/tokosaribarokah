
$(document).ready(function(){

get_master_multipayment();
  var table = $('#dt-history').DataTable( {
    "ajax": base_url+"History/get_data/",
    "order": [[ 0, 'desc' ]], 
    "columns": [
    { "data": "kode_transaksi" },
    { "data": "jenis_transaksi" },
    { "data": "tgl_transaksi" },
    {
      "data": null,
      render:function(data, type, row)
      {
       
        return 'Rp. '+numeral(data.red>0 ? data.red : data.add).format();
                               
      },
      "createdCell": function (td, cellData, rowData, row, col) {
        if ( cellData.red > 0 ) {
          $(td).attr({'class':'alert alert-danger'})
        }else{
          $(td).attr({'class':'alert alert-success'})
        }
      },
      "bSortable": false,
      "targets": -1
  },
    {
      "data": null,
      render:function(data, type, row)
      {
       
        return 'Rp. '+numeral(data.total).format();
                               
      },
      "bSortable": false,
      "targets": -1
  },
    {
      "data": null,
      render:function(data, type, row)
      {

        return "<button data-id='"+data.kode_transaksi+"' class='btn btn-default btn-sm btn-detail'><span class='btn-label'><i class='fa fa-eye'></i></span>  </button>"

      },
      "bSortable": false,
      "targets": -1
    },

    ],

  } );

  $('#btn-add').click(function(){
    console.log('run....')
    show_hide_card()
    $('.form-title').text('ADD DATA PULSA')
  })


  $('#btn-search').on('click',function(){
    console.log('run....')
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var kode_multipayment = $('#type_multipayment :checked').val();

       // response['data'][0].NAME
       $.ajax({
        url:base_url+"History/get_data_by_date/",
        type:'POST',
        data:{start_date,end_date,kode_multipayment},
        dataType:'json',
        success:function(response){
          console.log(response);
          table.clear().draw();
          var btn_print = $('#btn-print-all');
          var type_multipayment = $('#type_multipayment :checked').text();
          var btn_print_href = base_url+'laporan/all_transaction'+'/'+kode_multipayment+'/'+type_multipayment+'/'+end_date+'/'+end_date;
          if (response.data.length < 1) {
            btn_print_href = base_url+'History/#';
            $(btn_print).removeAttr('href target');
            alert('Data Tidak Ditemukan');
            return false;
           }  
          
           $(btn_print).removeAttr('href').attr({'href':btn_print_href,'target':'_blank'});
          $.each(response.data, function() {
           table.row.add(this).draw();
         });


        },error:function(response){

          console.log(response);
        }
        
      });

     })


  $('#dt-history').on('click','.btn-detail',function(){
    console.log('run....')
    var kode_transaksi = $(this).attr('data-id');
       // response['data'][0].NAME
       $.ajax({
        url:base_url+"History/get_data_by_code/"+kode_transaksi,
        type:'GET',
        dataType:'json',
        success:function(response){
          var field = $('.txt-input');
          $('input[name=kode_transaksi]').val(response['data'].kode_transaksi)
          $('input[name=nm_konsumen]').val(response['data'].nm_konsumen)
          $('input[name=jenis_transaksi]').val(response['data'].nm_multipayment)
          $('input[name=tgl_transaksi]').val(response['data'].tgl_transaksi)
          $('input[name=harga]').val(response['data'].harga)
          show_hide_card()
          $('.form-title').text('EDIT DATA PULSA')
        },error:function(response){

          console.log(response);
        }
        
      });

     })

  $('#btn-cancel').click(function(){
    console.log('run....')
    show_hide_card()
    $('.form-horizontal')[0].reset();
  })

  $('#btn-clear').click(function(){
    console.log('run....')
    $('.txt-input:not(input[name=kode_pulsa])').val('')
        //$('.form-horizontal')[0].reset();
      })
  $('#example').on('click','.status',function(){
    console.log('run....')
    admin_code = $(this).attr('data-id');
    mySpace.status = $(this).attr('data-status');
    alert_confirm('Are You Sure??',change_status);
  })


  $('#btn-save').click(function(){
    // var field = $('.txt_input');
    // for (var i = 1; i < field.length; i++) {
    //  var strErr = $(field[i]).parents()[2].innerText.replace("*", "");
    //   if (field[i].value === '') {
    //     alert_info('Isi Field :  '+strErr);
    //     field[i].classList.add('error_class');
    //     return false;
    //   }else{
    //     field[i].classList.remove('error_class');
    //   }
    // }
    var data = $("#form-pulsa").serializeArray();
    var array_data =[];
    array_data[0] = {};
    $.each(data,function(){
      array_data[0][this.name] = this.value;
    });
    ajax_save(array_data);
    
  });

  function ajax_save(array_data){
    $.ajax({
      url:base_url+"pulsa/save",
      type:'POST',
      data:{array_data},
      dataType:'json',
      success:function(response){
        if (response == true) {
         alert('Berhasil Simpan!!');
         show_hide_card();
         $('#dt-pulsa').DataTable().ajax.reload()
         $('.form-horizontal')[0].reset();
       } else{ 
         alert('Gagal Simpan!!');
       }

     },error:function(response){

      console.log(response);
    }
  });
  }


   function get_master_multipayment(array_data){
    $.ajax({
      url:base_url+"History/get_master_multipayment",
      type:'GET',
      dataType:'json',
      success:function(response){
       
       for (var i = 0; i < response.data.length; i++) {
         var data = '<option value = "'+response.data[i].kode_multipayment+'" >'+response.data[i].nama_multipayment+'</option>';
           $('#type_multipayment').append(data);

       }

     },error:function(response){

      console.log(response);
    }
  });
  }

})