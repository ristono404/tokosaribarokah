
$(document).ready(function(){

  var table = $('#dt-tagihan_listrik').DataTable( {
    "ajax": base_url+"Tagihan_Listrik/get_data/",
    "columns": [
    { "data": "kode_tagihan" },
    { "data": "no_meter" },
    { "data": "id_konsumen" },
    { "data": "tgl_transaksi" },
    { "data": "periode" },
    { "data": "harga" },
    { "data": "kwh" },
    {
      "data": null,
      render:function(data, type, row)
      {

        return "<button data-id='"+data.kode_tagihan+"' class='btn btn-default btn-sm btn_edit'><span class='btn-label'><i class='fa fa-pencil'></i></span>  </button>"+
        " <a data-id='"+data.kode_tagihan+"' href='"+base_url+"laporan/tagihan_listrik/"+data.kode_tagihan+"' target='_blank'  class='btn btn-info btn-sm'><span class='btn-label'><i class='fa fa-print'></i></span></a>"

      },
      "bSortable": false,
      "targets": -1
    },
            // {
            //     "data": null,
            //     render:function(data, type, row)
            //     {
            //       return "<center><button data-id='"+data.ADMIN_CODE+"' class='btn btn-default btn-sm btn_edit'><span class='btn-label'><i class='fa fa-pencil'></i></span>  Edit</button>"
            //     },
            //     "bSortable": false,
            //     "targets": -1
            // }
            ],

          } );
  $('#btn_add').click(function(){
    console.log('run....')
    show_hide_card()
    $('.form-title').text('ADD ADMIN DATA')
  })

  $('#dt-tagihan_listrik').on('click','.btn_edit',function(){
    console.log('run....')
    var tagihan_code = $(this).attr('data-id');
       // response['data'][0].NAME
       $.ajax({
        url:base_url+"Tagihan_Listrik/get_data_by_code/"+tagihan_code,
        type:'GET',
        dataType:'json',
        success:function(response){
          var field = $('.txt-input');
          field[0].value = response['data'].kode_tagihan
          field[1].value = response['data'].id_konsumen
          field[2].value = response['data'].no_meter
          field[3].value = response['data'].kwh
          field[4].value = response['data'].tgl_transaksi
          field[5].value = response['data'].periode
          field[6].value = response['data'].harga
          
          show_hide_card();
          $('.form-title').text('EDIT ADMIN DATA')
        },error:function(response){

          console.log(response);
        }
        
      });

     })

  $('#btn_cancel').click(function(){
    console.log('run....')
    show_hide_card()
    $('.form-horizontal')[0].reset();
  })

  $('#btn_clear').click(function(){
    console.log('run....')
    $('.form-horizontal')[0].reset();
  })
  $('#example').on('click','.status',function(){
    console.log('run....')
    admin_code = $(this).attr('data-id');
    mySpace.status = $(this).attr('data-status');
    alert_confirm('Are You Sure??',change_status);
  })


  $('#btn-save').on('click',function(){
    var valid = true;
    var v_list_data = $('#form-tagihan_listrik').serializeArray();
    var v_data = [];
    v_data.push({
      'username' : 'cek'
    });
    $.each(v_list_data,function(){
      if (this.value == "") {
        alert('Isi terlebih dahulu field '+this.name);
        valid = false;
        return false;
      }
      v_data[0][this.name] = this.value;
    });

    if (valid) {
      ajax_save(v_data);
    }
  });
  
  
  
//   $('#btnEditData').click(function(){
//     $('.edit_form').prop('disabled',false);
//     $('#id-confirm-password').attr('type','password');
//     $('#id-inp-password').val('');
//   });

function change_status(){
  var status = mySpace.status
  if(status==0){
    status = 1
  }else{
    status =0
  }
  var data = {admin_code : admin_code,status : status};
  var array_data =[];

  array_data.push({
   admin_code : data.admin_code,
   status : data.status
 });

  ajax_change_status(array_data)
}
function ajax_change_status(array_data){
  $.ajax({
    url:base_url+"Controller_Home_A/change_status",
    type:'POST',
    data:{array_data},
    dataType:'json',
    success:function(response){
      if (response == true) {
       alert_info('Berhasil Ubah!!');
       $('#example').DataTable().ajax.reload()
     } else{ 
       alert_info('Gagal Simpan!!');
     }

   },error:function(response){

    console.log(response);
  }
})
}

function ajax_save(array_data){
  $.ajax({
    url:base_url+"Tagihan_Listrik/save",
    type:'POST',
    data:{array_data},
    dataType:'json',
    success:function(response){
      if (response.status == '200') {
       alert('Berhasil Simpan!!');
        if (response.kode_trans != null){
            window.open(base_url+'laporan/tagihan_listrik/'+response.kode_trans+'');
         }
       show_hide_card();
       $('#dt-tagihan_listrik').DataTable().ajax.reload();
       $('.form-horizontal')[0].reset();
     } else{ 
       alert('Gagal Simpan!!');
       console.log(response);
     }

   },error:function(response){
    alert('Gagal Error!!'+response);
    console.log(response);
  }
});
}

})