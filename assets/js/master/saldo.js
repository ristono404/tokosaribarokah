
$(document).ready(function(){
    var table = $('#dt-saldo').DataTable( {
        "ajax": base_url+"saldo/get_data/",
        "order": [[ 0, 'desc' ]],
        "columns": [
            { "data": "kode_saldo" },
            {
              "data": null,
              render:function(data, type, row)
              {
               
                return 'Rp. '+numeral(data.jumlah_saldo).format();
                                       
              },
              "bSortable": false,
              "targets": -1
          },
            { "data": "tgl_transaksi" },
            {
                "data": null,
                render:function(data, type, row)
                {
                 
                  return "<button data-id='"+data.kode_saldo+"' class='btn btn-default btn-sm btn-edit'><span class='btn-label'><i class='fa fa-pencil'></i></span>  </button>"
                                         
                },
                "bSortable": false,
                "targets": -1
            },
          
        ],
       
    } );
   
    $('#btn-add').click(function(){
        console.log('run....')
        show_hide_card()
        $('.form-title').text('ADD DATA SALDO')
     })
     
     $('#datatable').on('click','.btn-edit',function(){
        console.log('run....')
        var kode_saldo = $(this).attr('data-id');
       // response['data'][0].NAME
       $.ajax({
        url:base_url+"saldo/get_data_by_code/"+kode_saldo,
        type:'GET',
        dataType:'json',
        success:function(response){
          var field = $('.txt-input');
           $('input[name=kode_saldo]').val(response['data'].kode_saldo)
           $('input[name=jumlah_saldo]').val(response['data'].jumlah_saldo)
           $('input[name=tgl_transaksi]').val(response['data'].tgl_transaksi)
          show_hide_card()
        $('.form-title').text('EDIT DATA SALDO')
        },error:function(response){
    
          console.log(response);
        }
        
      });
        
     })

     $('#btn-cancel').click(function(){
        console.log('run....')
        show_hide_card()
        $('.form-horizontal')[0].reset();
     })

     $('#btn-clear').click(function(){
        console.log('run....')
        $('.txt-input:not(input[name=kode_saldo])').val('')
        //$('.form-horizontal')[0].reset();
     })
     $('#example').on('click','.status',function(){
      console.log('run....')
      admin_code = $(this).attr('data-id');
      mySpace.status = $(this).attr('data-status');
      alert_confirm('Are You Sure??',change_status);
    })

    
$('#btn-save').click(function(){
    // var field = $('.txt_input');
    // for (var i = 1; i < field.length; i++) {
    //  var strErr = $(field[i]).parents()[2].innerText.replace("*", "");
    //   if (field[i].value === '') {
    //     alert_info('Isi Field :  '+strErr);
    //     field[i].classList.add('error_class');
    //     return false;
    //   }else{
    //     field[i].classList.remove('error_class');
    //   }
    // }
    var data = $("#form-saldo").serializeArray();
    var array_data =[];
    array_data[0] = {};
    $.each(data,function(){
      array_data[0][this.name] = this.value;
    });
   ajax_save(array_data);
    
  });

  function ajax_save(array_data){
    $.ajax({
      url:base_url+"saldo/save",
      type:'POST',
      data:{array_data},
      dataType:'json',
      success:function(response){
        if (response == true) {
         alert('Berhasil Simpan!!');
         show_hide_card();
         $('#dt-saldo').DataTable().ajax.reload()
         $('.form-horizontal')[0].reset();
       } else{ 
         alert('Gagal Simpan!!');
       }
  
     },error:function(response){
  
      console.log(response);
    }
  });
  }

})