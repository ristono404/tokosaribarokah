
$(document).ready(function(){
 
    var table = $('#dt-pulsa').DataTable( {
        "ajax": base_url+"pulsa/get_data/",
        "order": [[ 0, 'desc' ]],
        "columns": [
            { "data": "kode_pulsa" },
            { "data": "nm_konsumen" },
            {
              "data": null,
              render:function(data, type, row)
              {
               
                return 'Rp. '+numeral(data.isi_pulsa).format();
                                       
              },
              "bSortable": false,
              "targets": -1
          },
            { "data": "tgl_transaksi" },
            { "data": "no_telepon" },
            {
              "data": null,
              render:function(data, type, row)
              {
               
                return 'Rp. '+numeral(data.harga).format();
                                       
              },
              "bSortable": false,
              "targets": -1
            },
            {
                "data": null,
                render:function(data, type, row)
                {
                 
                  return "<button data-id='"+data.kode_pulsa+"' class='btn btn-default btn-sm btn-edit'><span class='btn-label'><i class='fa fa-pencil'></i></span>  </button> &nbsp;"+
                         "<a href='"+base_url+'laporan/pulsa_paket_data/'+data.kode_pulsa+'/pulsa'+"' target='_blank' class='btn btn-info btn-sm'><span class='btn-label'><i class='fa fa-print'></i></span>  </a>"
                
                },
                "bSortable": false,
                "targets": -1
            },
          
        ],
       
    } );
   
    $('#btn-add').click(function(){
        console.log('run....')
        show_hide_card()
        $('.form-title').text('ADD DATA PULSA')
     })
     
     $('#datatable').on('click','.btn-edit',function(){
        console.log('run....')
        var kode_pulsa = $(this).attr('data-id');
       // response['data'][0].NAME
       $.ajax({
        url:base_url+"pulsa/get_data_by_code/"+kode_pulsa,
        type:'GET',
        dataType:'json',
        success:function(response){
          var field = $('.txt-input');
          $('input[name=kode_pulsa]').val(response['data'].kode_pulsa)
           $('select[name=id_konsumen]').val(response['data'].id_konsumen)
          $('input[name=no_telepon]').val(response['data'].no_telepon)
          $('input[name=isi_pulsa]').val(response['data'].isi_pulsa)
          $('input[name=harga]').val(response['data'].harga)
          $('input[name=tgl_transaksi]').val(response['data'].tgl_transaksi)
          show_hide_card()
        $('.form-title').text('EDIT DATA PULSA')
        },error:function(response){
    
          console.log(response);
        }
        
      });
        
     })

     $('#btn-cancel').click(function(){
        console.log('run....')
        show_hide_card()
        $('.form-horizontal')[0].reset();
     })

     $('#btn-clear').click(function(){
        console.log('run....')
        $('.txt-input:not(input[name=kode_pulsa])').val('')
        //$('.form-horizontal')[0].reset();
     })
     $('#example').on('click','.status',function(){
      console.log('run....')
      admin_code = $(this).attr('data-id');
      mySpace.status = $(this).attr('data-status');
      alert_confirm('Are You Sure??',change_status);
    })

    
$('#btn-save').click(function(){
    // var field = $('.txt_input');
    // for (var i = 1; i < field.length; i++) {
    //  var strErr = $(field[i]).parents()[2].innerText.replace("*", "");
    //   if (field[i].value === '') {
    //     alert_info('Isi Field :  '+strErr);
    //     field[i].classList.add('error_class');
    //     return false;
    //   }else{
    //     field[i].classList.remove('error_class');
    //   }
    // }
    var data = $("#form-pulsa").serializeArray();
    var array_data =[];
    array_data[0] = {};
     var valid = true;
    $.each(data,function(){
      if (this.value == "") {
        alert('Isi terlebih dahulu field '+this.name);
        valid = false;
        return false;
      }
      array_data[0][this.name] = this.value;
    });

    if (valid) {
      ajax_save(array_data);
    }
  });

  function ajax_save(array_data){
    $.ajax({
      url:base_url+"pulsa/save",
      type:'POST',
      data:{array_data},
      dataType:'json',
      success:function(response){
        if (response.status == true) {
         alert('Berhasil Simpan!!');
         if (response.kode_trans != null){
            window.open(base_url+'laporan/pulsa_paket_data/'+response.kode_trans+'/pulsa');
         }
         show_hide_card();
         $('#dt-pulsa').DataTable().ajax.reload()
         $('.form-horizontal')[0].reset();
       } else{ 
         alert('Gagal Simpan!!');
       }
  
     },error:function(response){
  
      console.log(response);
    }
  });
  }

})