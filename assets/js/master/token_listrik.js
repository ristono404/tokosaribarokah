
$(document).ready(function(){
 
    var table = $('#dt-token-listrik').DataTable( {
        "ajax": base_url+"token_listrik/get_data/",
        "columns": [
            { "data": "kode_token" },
            { "data": "nm_konsumen" },
            { "data": "no_konsumen" },
            { "data": "tgl_transaksi" },
            { "data": "no_seri" },
            { "data": "harga" },
            { "data": "kwh" },
            {
                "data": null,
                render:function(data, type, row)
                {
                 
                  return "<button data-id='"+data.kode_token+"' class='btn btn-default btn-sm btn-edit'><span class='btn-label'><i class='fa fa-pencil'></i></span>  </button>"+
                         " <a href='"+base_url+"laporan/token_listrik/"+data.kode_token+"' target='_blank' class='btn btn-info btn-sm btn-delete'><span class='btn-label'><i class='fa fa-print'></i></span></a>"
                
                },
                "bSortable": false,
                "targets": -1
            },
          
        ],
       
    } );
   
    $('#btn-add').click(function(){
        console.log('run....')
        show_hide_card()
        $('.form-title').text('ADD DATA TOKEN LISTRIK')
     })
     
     $('#datatable').on('click','.btn-edit',function(){
        console.log('run....')
        var kode_token = $(this).attr('data-id');
       // response['data'][0].NAME
       $.ajax({
        url:base_url+"token_listrik/get_data_by_code/"+kode_token,
        type:'GET',
        dataType:'json',
        success:function(response){
          var field = $('.txt-input');
          $('input[name=kode_token]').val(response['data'].kode_token)
          $('select[name=id_konsumen]').val(response['data'].id_konsumen)
          $('input[name=no_konsumen]').val(response['data'].no_konsumen)
          $('input[name=tgl_transaksi]').val(response['data'].tgl_transaksi)
          $('input[name=no_seri]').val(response['data'].no_seri)
          $('input[name=harga]').val(response['data'].harga)
          $('input[name=kwh]').val(response['data'].kwh)
          show_hide_card()
        $('.form-title').text('EDIT DATA TOKEN LISTRIK')
        },error:function(response){
    
          console.log(response);
        }
        
      });
        
     })

     $('#btn-cancel').click(function(){
        console.log('run....')
        show_hide_card()
        $('.form-horizontal')[0].reset();
     })

     $('#btn-clear').click(function(){
        console.log('run....')
        $('.txt-input:not(input[name=kode_token])').val('')
        //$('.form-horizontal')[0].reset();
     })
     $('#example').on('click','.status',function(){
      console.log('run....')
      admin_code = $(this).attr('data-id');
      mySpace.status = $(this).attr('data-status');
      alert_confirm('Are You Sure??',change_status);
    })

    
$('#btn-save').click(function(){
    // var field = $('.txt_input');
    // for (var i = 1; i < field.length; i++) {
    //  var strErr = $(field[i]).parents()[2].innerText.replace("*", "");
    //   if (field[i].value === '') {
    //     alert_info('Isi Field :  '+strErr);
    //     field[i].classList.add('error_class');
    //     return false;
    //   }else{
    //     field[i].classList.remove('error_class');
    //   }
    // }
    var data = $("#form-token-listrik").serializeArray();
    var array_data =[];
    array_data[0] = {};
     var valid = true;
    $.each(data,function(){
      if (this.value == "") {
        alert('Isi terlebih dahulu field '+this.name);
        valid = false;
        return false;
      }
      array_data[0][this.name] = this.value;
    });

    if (valid) {
      ajax_save(array_data);
    }
  });

  function ajax_save(array_data){
    $.ajax({
      url:base_url+"token_listrik/save",
      type:'POST',
      data:{array_data},
      dataType:'json',
      success:function(response){
        if (response.status == true) {
         alert('Berhasil Simpan!!');
          if (response.kode_trans != null){
            window.open(base_url+'laporan/token_listrik/'+response.kode_trans+'/pulsa');
         }
         show_hide_card();
         $('#dt-token-listrik').DataTable().ajax.reload()
         $('.form-horizontal')[0].reset();
       } else{ 
         alert('Gagal Simpan!!');
       }
  
     },error:function(response){
  
      console.log(response);
    }
  });
  }

})