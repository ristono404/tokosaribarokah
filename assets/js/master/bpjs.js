
$(document).ready(function(){
 
    var table = $('#dt-bpjs').DataTable( {
        "ajax": base_url+"bpjs/get_data/",
        "columns": [
            { "data": "kode_bpjs" },
            { "data": "periode" },
            { "data": "nm_konsumen" },
            { "data": "tgl_transaksi" },
            { "data": "no_reff" },
            { 
              "data": null,
              render:function(data, type, row)
              {
               
                return 'Rp. '+numeral(data.harga).format();
                                       
              },
              "bSortable": false,
              "targets": -1
          },
            {
                "data": null,
                render:function(data, type, row)
                {
                 
                  return "<button data-id='"+data.kode_bpjs+"' class='btn btn-default btn-sm btn-edit'><span class='btn-label'><i class='fa fa-pencil'></i></span>  </button>"+
                         "<a href='"+base_url+'laporan/bpjs/'+data.kode_bpjs+"' target='_blank' class='btn btn-info btn-sm'><span class='btn-label'><i class='fa fa-print'></i></span>  </a>"
                
                },
                "bSortable": false,
                "targets": -1
            },
          
        ],
       
    } );
   
    $('#btn-add').click(function(){
        console.log('run....')
        show_hide_card()
        $('.form-title').text('ADD DATA BPJS')
     })
     
     $('#datatable').on('click','.btn-edit',function(){
        console.log('run....')
        var kode_bpjs = $(this).attr('data-id');
       // response['data'][0].NAME
       $.ajax({
        url:base_url+"bpjs/get_data_by_code/"+kode_bpjs,
        type:'GET',
        dataType:'json',
        success:function(response){
          var field = $('.txt-input');
          $('input[name=kode_bpjs]').val(response['data'].kode_bpjs)
           $('select[name=id_konsumen]').val(response['data'].id_konsumen)
          $('input[name=periode]').val(response['data'].periode)
          $('input[name=id_konsumen]').val(response['data'].id_konsumen )
          $('input[name=tgl_transaksi]').val(response['data'].tgl_transaksi)
          $('input[name=no_reff]').val(response['data'].no_reff)
          $('input[name=harga]').val(response['data'].harga)
          show_hide_card()
        $('.form-title').text('EDIT DATA BPJS')
        },error:function(response){
    
          console.log(response);
        }
        
      });
        
     })

     $('#btn-cancel').click(function(){
        console.log('run....')
        show_hide_card()
        $('.form-horizontal')[0].reset();
     })

     $('#btn-clear').click(function(){
        console.log('run....')
        $('.txt-input:not(input[name=kode_bpjs])').val('')
        //$('.form-horizontal')[0].reset();
     })
     $('#example').on('click','.status',function(){
      console.log('run....')
      admin_code = $(this).attr('data-id');
      mySpace.status = $(this).attr('data-status');
      alert_confirm('Are You Sure??',change_status);
    })

    
$('#btn-save').click(function(){
    var data = $("#form-bpjs").serializeArray();
    var array_data =[];
    array_data[0] = {};
     var valid = true;
    $.each(data,function(){
      if (this.value == "") {
        alert('Isi terlebih dahulu field '+this.name);
        valid = false;
        return false;
      }
      array_data[0][this.name] = this.value;
    });

    if (valid) {
      ajax_save(array_data);
    }
  });

  function ajax_save(array_data){
    $.ajax({
      url:base_url+"bpjs/save",
      type:'POST',
      data:{array_data},
      dataType:'json',
      success:function(response){
        if (response.status == true) {
         alert('Berhasil Simpan!!');
         show_hide_card();
         if (response.kode_bpjs != null){
             window.open(base_url+'laporan/bpjs/'+response.kode_bpjs);
         }
         
         $('#dt-bpjs').DataTable().ajax.reload()
         $('.form-horizontal')[0].reset();
       } else{ 
         alert('Gagal Simpan!!');
       }
  
     },error:function(response){
  
      console.log(response);
    }
  });
  }

})