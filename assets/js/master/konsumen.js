
$(document).ready(function(){
 
    var table = $('#dt-konsumen').DataTable( {
        "ajax": base_url+"konsumen/get_data/",
        "columns": [
            { "data": "kode_konsumen" },
            { "data": "nm_konsumen" },
            { "data": "no_telepon" },
            {
                "data": null,
                render:function(data, type, row)
                {
                 
                  return "<button data-id='"+data.kode_konsumen+"' class='btn btn-default btn-sm btn-edit'><span class='btn-label'><i class='fa fa-pencil'></i></span>  </button>"+
                         " <a href='"+base_url+"konsumen/delete/"+data.kode_konsumen+"' class='btn btn-danger btn-sm btn-delete'><span class='btn-label'><i class='fa fa-trash'></i></span></a>"
                
                },
                "bSortable": false,
                "targets": -1
            },
          
        ],
       
    } );
   
    $('#btn-add').click(function(){
        console.log('run....')
        show_hide_card()
        $('.form-title').text('ADD DATA KONSUMEN')
     })
     
     $('#datatable').on('click','.btn-edit',function(){
        console.log('run....')
        var kode_konsumen = $(this).attr('data-id');
       // response['data'][0].NAME
       $.ajax({
        url:base_url+"konsumen/get_data_by_code/"+kode_konsumen,
        type:'GET',
        dataType:'json',
        success:function(response){
          var field = $('.txt-input');
          $('input[name=kode_konsumen]').val(response['data'].kode_konsumen)
          $('input[name=nm_konsumen]').val(response['data'].nm_konsumen)
          $('input[name=no_telepon]').val(response['data'].no_telepon)
          show_hide_card()
        $('.form-title').text('EDIT DATA KONSUMEN')
        },error:function(response){
    
          console.log(response);
        }
        
      });
        
     })

     $('#btn-cancel').click(function(){
        console.log('run....')
        show_hide_card()
        $('.form-horizontal')[0].reset();
     })

     $('#btn-clear').click(function(){
        console.log('run....')
        $('.txt-input:not(input[name=kode_konsumen])').val('')
        //$('.form-horizontal')[0].reset();
     })
     $('#example').on('click','.status',function(){
      console.log('run....')
      admin_code = $(this).attr('data-id');
      mySpace.status = $(this).attr('data-status');
      alert_confirm('Are You Sure??',change_status);
    })

    
$('#btn-save').click(function(){
    // var field = $('.txt_input');
    // for (var i = 1; i < field.length; i++) {
    //  var strErr = $(field[i]).parents()[2].innerText.replace("*", "");
    //   if (field[i].value === '') {
    //     alert_info('Isi Field :  '+strErr);
    //     field[i].classList.add('error_class');
    //     return false;
    //   }else{
    //     field[i].classList.remove('error_class');
    //   }
    // }
    var data = $("#form-konsumen").serializeArray();
    var array_data =[];
    array_data[0] = {};
    var valid = true;
    $.each(data,function(){
      if (this.value == "") {
        alert('Isi terlebih dahulu field '+this.name);
        valid = false;
        return false;
      }
      array_data[0][this.name] = this.value;
    });

    if (valid) {
      ajax_save(array_data);
    }
      
  });

  function ajax_save(array_data){
    $.ajax({
      url:base_url+"konsumen/save",
      type:'POST',
      data:{array_data},
      dataType:'json',
      success:function(response){
        if (response == true) {
         alert('Berhasil Simpan!!');
         show_hide_card();
         $('#dt-konsumen').DataTable().ajax.reload()
         $('.form-horizontal')[0].reset();
       } else{ 
         alert('Gagal Simpan!!');
       }
  
     },error:function(response){
  
      console.log(response);
    }
  });
  }

})