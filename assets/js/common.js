var base_url = $('#base_url').val();

function show_hide_card(){
    var datatable = $('#datatable');
    var form = $('#form');

    if(datatable[0].style.display == "none"){
        datatable[0].style.display = "block"
        form[0].style.display = "none"
    }else{
        datatable[0].style.display = "none"
        form[0].style.display = "block"
    }
}