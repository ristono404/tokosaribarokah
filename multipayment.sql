-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.6.21 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5174
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for multipayment
CREATE DATABASE IF NOT EXISTS `multipayment` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `multipayment`;

-- Dumping structure for table multipayment.tbl_m_admin
CREATE TABLE IF NOT EXISTS `tbl_m_admin` (
  `id_admin` varchar(5) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table multipayment.tbl_m_admin: ~1 rows (approximately)
DELETE FROM `tbl_m_admin`;
/*!40000 ALTER TABLE `tbl_m_admin` DISABLE KEYS */;
INSERT INTO `tbl_m_admin` (`id_admin`, `username`, `password`) VALUES
	('A001', 'hazard', '123123');
/*!40000 ALTER TABLE `tbl_m_admin` ENABLE KEYS */;

-- Dumping structure for table multipayment.tbl_m_air_pdam
CREATE TABLE IF NOT EXISTS `tbl_m_air_pdam` (
  `kode_pdam` varchar(20) NOT NULL,
  `no_pdam` varchar(20) NOT NULL,
  `id_konsumen` varchar(15) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `periode` date NOT NULL,
  `harga` int(10) NOT NULL,
  PRIMARY KEY (`kode_pdam`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table multipayment.tbl_m_air_pdam: ~1 rows (approximately)
DELETE FROM `tbl_m_air_pdam`;
/*!40000 ALTER TABLE `tbl_m_air_pdam` DISABLE KEYS */;
INSERT INTO `tbl_m_air_pdam` (`kode_pdam`, `no_pdam`, `id_konsumen`, `tgl_transaksi`, `periode`, `harga`) VALUES
	('KP001', '1231231231', 'KN002', '2019-06-29', '2019-06-15', 50000);
/*!40000 ALTER TABLE `tbl_m_air_pdam` ENABLE KEYS */;

-- Dumping structure for table multipayment.tbl_m_bpjs
CREATE TABLE IF NOT EXISTS `tbl_m_bpjs` (
  `kode_bpjs` varchar(20) NOT NULL,
  `periode` date NOT NULL,
  `id_konsumen` varchar(20) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `no_reff` varchar(15) NOT NULL,
  `harga` int(10) NOT NULL,
  PRIMARY KEY (`kode_bpjs`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table multipayment.tbl_m_bpjs: ~1 rows (approximately)
DELETE FROM `tbl_m_bpjs`;
/*!40000 ALTER TABLE `tbl_m_bpjs` DISABLE KEYS */;
INSERT INTO `tbl_m_bpjs` (`kode_bpjs`, `periode`, `id_konsumen`, `tgl_transaksi`, `no_reff`, `harga`) VALUES
	('BP001', '2019-06-29', 'KN002', '2019-06-29', '2034732748', 100000);
/*!40000 ALTER TABLE `tbl_m_bpjs` ENABLE KEYS */;

-- Dumping structure for table multipayment.tbl_m_konsumen
CREATE TABLE IF NOT EXISTS `tbl_m_konsumen` (
  `kode_konsumen` varchar(5) NOT NULL,
  `nm_konsumen` varchar(40) NOT NULL,
  `no_telepon` varchar(20) NOT NULL,
  PRIMARY KEY (`kode_konsumen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table multipayment.tbl_m_konsumen: ~2 rows (approximately)
DELETE FROM `tbl_m_konsumen`;
/*!40000 ALTER TABLE `tbl_m_konsumen` DISABLE KEYS */;
INSERT INTO `tbl_m_konsumen` (`kode_konsumen`, `nm_konsumen`, `no_telepon`) VALUES
	('KN002', 'TONO', '08121212'),
	('KN003', 'tono 2', '11');
/*!40000 ALTER TABLE `tbl_m_konsumen` ENABLE KEYS */;

-- Dumping structure for table multipayment.tbl_m_multipayment
CREATE TABLE IF NOT EXISTS `tbl_m_multipayment` (
  `kode_multipayment` varchar(20) NOT NULL,
  `nama_multipayment` varchar(20) NOT NULL,
  `table_name` varchar(40) NOT NULL,
  `key` varchar(20) NOT NULL,
  PRIMARY KEY (`kode_multipayment`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table multipayment.tbl_m_multipayment: ~8 rows (approximately)
DELETE FROM `tbl_m_multipayment`;
/*!40000 ALTER TABLE `tbl_m_multipayment` DISABLE KEYS */;
INSERT INTO `tbl_m_multipayment` (`kode_multipayment`, `nama_multipayment`, `table_name`, `key`) VALUES
	('MP001', 'ISI SALDO', 'tbl_t_saldo', 'kode_saldo'),
	('MP002', 'PULSA', 'tbl_m_pulsa', 'kode_pulsa'),
	('MP003', 'TAGIHAN LISTRIK', 'tbl_m_tagihan_listrik', 'kode_tagihan'),
	('MP004', 'AIR PDAM', 'tbl_m_air_pdam', 'kode_pdam'),
	('MP005', 'BPJS', 'tbl_m_bpjs', 'kode_bpjs'),
	('MP006', 'PULSA', 'tbl_m_pulsa', 'kode_pulsa'),
	('MP007', 'TOKEN LISTRIK', 'tbl_m_token_listrik', 'kode_token'),
	('MP008', 'PAKET DATA', 'tbl_m_paket_data', 'kode_paket');
/*!40000 ALTER TABLE `tbl_m_multipayment` ENABLE KEYS */;

-- Dumping structure for table multipayment.tbl_m_paket_data
CREATE TABLE IF NOT EXISTS `tbl_m_paket_data` (
  `kode_paket_data` varchar(10) NOT NULL,
  `isi_paket` int(7) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `no_telepon` varchar(20) NOT NULL,
  `harga` int(10) NOT NULL,
  `id_konsumen` varchar(10) NOT NULL,
  PRIMARY KEY (`kode_paket_data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table multipayment.tbl_m_paket_data: ~1 rows (approximately)
DELETE FROM `tbl_m_paket_data`;
/*!40000 ALTER TABLE `tbl_m_paket_data` DISABLE KEYS */;
INSERT INTO `tbl_m_paket_data` (`kode_paket_data`, `isi_paket`, `tgl_transaksi`, `no_telepon`, `harga`, `id_konsumen`) VALUES
	('PD001', 40000, '2019-06-20', '08989898989', 60000, 'KN002');
/*!40000 ALTER TABLE `tbl_m_paket_data` ENABLE KEYS */;

-- Dumping structure for table multipayment.tbl_m_pulsa
CREATE TABLE IF NOT EXISTS `tbl_m_pulsa` (
  `kode_pulsa` varchar(20) NOT NULL,
  `isi_pulsa` int(7) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `no_telepon` varchar(20) NOT NULL,
  `harga` int(10) NOT NULL,
  `id_konsumen` varchar(10) NOT NULL,
  PRIMARY KEY (`kode_pulsa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table multipayment.tbl_m_pulsa: ~2 rows (approximately)
DELETE FROM `tbl_m_pulsa`;
/*!40000 ALTER TABLE `tbl_m_pulsa` DISABLE KEYS */;
INSERT INTO `tbl_m_pulsa` (`kode_pulsa`, `isi_pulsa`, `tgl_transaksi`, `no_telepon`, `harga`, `id_konsumen`) VALUES
	('PL001', 20000, '2019-06-29', '0293023', 20000, 'KN002'),
	('PL002', 10000, '2019-06-29', '0293023', 20000, 'KN002');
/*!40000 ALTER TABLE `tbl_m_pulsa` ENABLE KEYS */;

-- Dumping structure for table multipayment.tbl_m_tagihan_listrik
CREATE TABLE IF NOT EXISTS `tbl_m_tagihan_listrik` (
  `kode_tagihan` varchar(20) NOT NULL,
  `no_meter` varchar(20) NOT NULL,
  `id_konsumen` varchar(20) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `periode` date NOT NULL,
  `harga` int(11) NOT NULL,
  `kwh` double NOT NULL,
  PRIMARY KEY (`kode_tagihan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table multipayment.tbl_m_tagihan_listrik: ~1 rows (approximately)
DELETE FROM `tbl_m_tagihan_listrik`;
/*!40000 ALTER TABLE `tbl_m_tagihan_listrik` DISABLE KEYS */;
INSERT INTO `tbl_m_tagihan_listrik` (`kode_tagihan`, `no_meter`, `id_konsumen`, `tgl_transaksi`, `periode`, `harga`, `kwh`) VALUES
	('KT001', '234343243', 'KN002', '2019-06-29', '2019-06-27', 100000, 23);
/*!40000 ALTER TABLE `tbl_m_tagihan_listrik` ENABLE KEYS */;

-- Dumping structure for table multipayment.tbl_m_token_listrik
CREATE TABLE IF NOT EXISTS `tbl_m_token_listrik` (
  `kode_token` varchar(20) NOT NULL,
  `id_konsumen` varchar(10) NOT NULL,
  `no_konsumen` varchar(30) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `no_seri` varchar(20) NOT NULL,
  `harga` int(10) NOT NULL,
  `kwh` double NOT NULL,
  PRIMARY KEY (`kode_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table multipayment.tbl_m_token_listrik: ~1 rows (approximately)
DELETE FROM `tbl_m_token_listrik`;
/*!40000 ALTER TABLE `tbl_m_token_listrik` DISABLE KEYS */;
INSERT INTO `tbl_m_token_listrik` (`kode_token`, `id_konsumen`, `no_konsumen`, `tgl_transaksi`, `no_seri`, `harga`, `kwh`) VALUES
	('TK001', 'KN002', '490237492387', '2019-06-27', '4327984742894', 90000, 40);
/*!40000 ALTER TABLE `tbl_m_token_listrik` ENABLE KEYS */;

-- Dumping structure for table multipayment.tbl_t_saldo
CREATE TABLE IF NOT EXISTS `tbl_t_saldo` (
  `kode_saldo` varchar(20) NOT NULL,
  `jumlah_saldo` int(11) NOT NULL,
  PRIMARY KEY (`kode_saldo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table multipayment.tbl_t_saldo: ~1 rows (approximately)
DELETE FROM `tbl_t_saldo`;
/*!40000 ALTER TABLE `tbl_t_saldo` DISABLE KEYS */;
INSERT INTO `tbl_t_saldo` (`kode_saldo`, `jumlah_saldo`) VALUES
	('SD001', 500000);
/*!40000 ALTER TABLE `tbl_t_saldo` ENABLE KEYS */;

-- Dumping structure for table multipayment.tbl_t_transaksi
CREATE TABLE IF NOT EXISTS `tbl_t_transaksi` (
  `kode_transaksi` varchar(20) NOT NULL,
  `kode_multipayment` varchar(20) NOT NULL,
  `kode_submultipayment` varchar(20) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `add` int(11) NOT NULL,
  `red` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  PRIMARY KEY (`kode_transaksi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table multipayment.tbl_t_transaksi: ~8 rows (approximately)
DELETE FROM `tbl_t_transaksi`;
/*!40000 ALTER TABLE `tbl_t_transaksi` DISABLE KEYS */;
INSERT INTO `tbl_t_transaksi` (`kode_transaksi`, `kode_multipayment`, `kode_submultipayment`, `tgl_transaksi`, `add`, `red`, `total`) VALUES
	('TR001', 'MP001', 'SD001', '2019-06-29', 500000, 0, 500000),
	('TR002', 'MP003', 'KT001', '2019-06-29', 0, 100000, 400000),
	('TR003', 'MP004', 'KP001', '2019-06-29', 0, 50000, 350000),
	('TR004', 'MP008', 'PD001', '2019-06-20', 0, 60000, 290000),
	('TR005', 'MP007', 'TK001', '2019-06-27', 0, 90000, 200000),
	('TR006', 'MP005', 'BP001', '2019-06-29', 0, 100000, 100000),
	('TR007', 'MP006', 'PL001', '2019-06-29', 0, 20000, 80000),
	('TR008', 'MP006', 'PL002', '2019-06-29', 0, 10000, 70000);
/*!40000 ALTER TABLE `tbl_t_transaksi` ENABLE KEYS */;

-- Dumping structure for procedure multipayment.update_trans_air_pdam
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_trans_air_pdam`(
	IN `kode_air_pdam` VARCHAR(7),
	IN `deff` INT


)
BEGIN
      DECLARE kode_transaksi CHAR(8);
     
      SET @kode_transaksi = (SELECT trs.kode_transaksi
									FROM tbl_m_air_pdam tma
									INNER JOIN tbl_t_transaksi trs ON trs.kode_submultipayment=tma.kode_pdam
									WHERE tma.kode_pdam = kode_air_pdam );
		
		UPDATE tbl_t_transaksi trs 
		SET trs.total=(trs.total - (deff))
		WHERE trs.kode_transaksi>=@kode_transaksi;
     

END//
DELIMITER ;

-- Dumping structure for procedure multipayment.update_trans_bpjs
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_trans_bpjs`(
	IN `kode_bpjs` VARCHAR(7),
	IN `deff` INT


)
BEGIN
      DECLARE kode_transaksi CHAR(8);
     
      SET @kode_transaksi = (SELECT trs.kode_transaksi
									FROM tbl_m_bpjs tmb
									INNER JOIN tbl_t_transaksi trs ON trs.kode_submultipayment=tmb.kode_bpjs
									WHERE tmb.kode_bpjs = kode_bpjs );
		
		UPDATE tbl_t_transaksi trs 
		SET trs.total=(trs.total - (deff))
		WHERE trs.kode_transaksi>=@kode_transaksi;
     

END//
DELIMITER ;

-- Dumping structure for procedure multipayment.update_trans_paket_data
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_trans_paket_data`(
	IN `kode_paket_data` VARCHAR(7),
	IN `deff` INT


)
BEGIN
      DECLARE kode_transaksi CHAR(8);
     
      SET @kode_transaksi = (SELECT trs.kode_transaksi
									FROM tbl_m_paket_data tmp
									INNER JOIN tbl_t_transaksi trs ON trs.kode_submultipayment=tmp.kode_paket_data
									WHERE tmp.kode_paket_data = kode_paket_data );
		
		UPDATE tbl_t_transaksi trs 
		SET trs.total=(trs.total - (deff))
		WHERE trs.kode_transaksi>=@kode_transaksi;
     

END//
DELIMITER ;

-- Dumping structure for procedure multipayment.update_trans_pulsa
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_trans_pulsa`(
	IN `kode_pulsa` VARCHAR(7),
	IN `deff` INT







)
BEGIN
      DECLARE kode_transaksi CHAR(8);
     
      SET @kode_transaksi = (SELECT trs.kode_transaksi
									FROM tbl_m_pulsa tsd
									INNER JOIN tbl_t_transaksi trs ON trs.kode_submultipayment=tsd.kode_pulsa
									WHERE tsd.kode_pulsa=kode_pulsa);
									
		UPDATE tbl_t_transaksi trs 
		SET trs.total=(trs.total - (deff))
		WHERE trs.kode_transaksi>=@kode_transaksi;

END//
DELIMITER ;

-- Dumping structure for procedure multipayment.update_trans_saldo
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_trans_saldo`(
	IN `kode_saldo` VARCHAR(7),
	IN `deff` INT

)
BEGIN
      DECLARE kode_transaksi CHAR(8);
     
      SET @kode_transaksi = (SELECT trs.kode_transaksi
									FROM tbl_t_saldo tsd
									INNER JOIN tbl_t_transaksi trs ON trs.kode_submultipayment=tsd.kode_saldo
									WHERE tsd.kode_saldo=kode_saldo);
		
		UPDATE tbl_t_transaksi trs 
		SET trs.total=(trs.total + (deff))
		WHERE trs.kode_transaksi>=@kode_transaksi;
     

END//
DELIMITER ;

-- Dumping structure for procedure multipayment.update_trans_tagihan_listrik
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_trans_tagihan_listrik`(
	IN `kode_tagihan` VARCHAR(7),
	IN `deff` INT)
BEGIN
      DECLARE kode_transaksi CHAR(8);
     
      SET @kode_transaksi = (SELECT trs.kode_transaksi
									FROM tbl_m_tagihan_listrik  tml
									INNER JOIN tbl_t_transaksi trs ON trs.kode_submultipayment=tml.kode_tagihan
									WHERE tml.kode_tagihan = kode_tagihan);
									
		UPDATE tbl_t_transaksi trs 
		SET trs.total=(trs.total - (deff))
		WHERE trs.kode_transaksi>=@kode_transaksi;

END//
DELIMITER ;

-- Dumping structure for procedure multipayment.update_trans_token_listrik
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_trans_token_listrik`(
	IN `kode_token_listrik` VARCHAR(7),
	IN `deff` INT


)
BEGIN
      DECLARE kode_transaksi CHAR(8);
     
      SET @kode_transaksi = (SELECT trs.kode_transaksi
									FROM tbl_m_token_listrik tmt
									INNER JOIN tbl_t_transaksi trs ON trs.kode_submultipayment=tmt.kode_token
									WHERE tmt.kode_token = kode_token_listrik );
		
		UPDATE tbl_t_transaksi trs 
		SET trs.total=(trs.total - (deff))
		WHERE trs.kode_transaksi>=@kode_transaksi;
     

END//
DELIMITER ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
