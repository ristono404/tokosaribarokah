<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tagihan_Listrik extends CI_Controller {

    function __construct(){
		parent::__construct();
		$this->load->model('model_tagihan_listrik');
		$this->load->model('model_konsumen');
		$this->load->library('session');
		chek_session_admin();
	}
	public function index()
	{
		$data['js'] = "master/tagihan_listrik.js";
		$data['menu1'] = "Master";
		$data['menu2'] = "Tagihan Listrik";
        $data['data_konsumen'] = $this->model_konsumen->get_data();
        $this->load->view('template/header',$data);
        $this->load->view('tagihan_listrik');
		$this->load->view('template/footer',$data);
	}

	function get_data(){
		$data['data'] = $this->model_tagihan_listrik->get_data();
		$this->output->set_output(json_encode($data));
	}

	public function get_data_by_code($kode_tagihan){
		$data['data'] = $this->model_tagihan_listrik->get_data_by_code($kode_tagihan);
		$this->output->set_output(json_encode($data));
	}

	public function save(){
		$data = $this->input->post('array_data');
		$data = $this->model_tagihan_listrik->save($data);
		$this->output->set_output(json_encode($data));
	}

	public function update_data(){
        $kode_tagihan = $this->input->post('kode_tagihan');
		$data = $this->input->post('array_data');
		$data = $this->model_tagihan_listrik->update_data($data,$kode_tagihan);
		$this->output->set_output(json_encode($data));
	}
	public function delete_data($kode_tagihan){
		$data = $this->model_tagihan_listrik->delete_data($kode_tagihan);
		if ($data) {
			redirect('/Tagihan_Listrik');
		}
	}
}
