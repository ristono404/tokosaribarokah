<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct(){
		parent::__construct();
		// $this->load->model('model_admin');
		$this->load->library('session');
		chek_session_admin();
	}
	public function index()
	{
        $data['menu1'] = "Dashboard";
		$data['menu2'] = "Home";
        $this->load->view('template/header',$data);
          $this->load->view('view_home');
		$this->load->view('template/footer');
	}
}
