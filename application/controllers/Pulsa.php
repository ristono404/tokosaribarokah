<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pulsa extends CI_Controller {
    
    function __construct(){
		parent::__construct();
		$this->load->model('model_pulsa');
		$this->load->model('model_konsumen');
		$this->load->library('session');
	}

	public function index()
	{
		
		$data['js'] = "master/pulsa.js";
		$data['menu1'] = "Transaksi";
		$data['menu2'] = "Pulsa";
		 $data['data_konsumen'] = $this->model_konsumen->get_data();
        $this->load->view('template/header',$data);
        $this->load->view('master/pulsa/index');
	    $this->load->view('template/footer',$data);
    }
    
    function get_data(){
		$data['data'] = $this->model_pulsa->get_data();
		$this->output->set_output(json_encode($data));
	}
	public function get_data_by_code($kode_pulsa){
		$data['data'] = $this->model_pulsa->get_data_by_code($kode_pulsa);
		$this->output->set_output(json_encode($data));
	}

	function save(){
		$data = $this->input->post('array_data');
		// var_dump($data);
		// die;
		$data = $this->model_pulsa->save($data);
		$this->output->set_output(json_encode($data));
	}
	function delete($kode_pulsa)
	{
		$data = $this->model_pulsa->delete($kode_pulsa);
		redirect('pulsa');
	}

    
}
