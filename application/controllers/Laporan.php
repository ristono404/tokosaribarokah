<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
    
    function __construct(){
		parent::__construct();
		$this->load->model('model_laporan');
		$this->load->library('session');
	}

	public function all_transaction($kode_multipayment=null,$nama_multipayment=null,$start_date=null,$end_date=null)
	{
		$data['all_transaction'] = $start_date==null ? $this->model_laporan->get_all_transaction($kode_multipayment) :  $this->model_laporan->get_data_by_date($start_date,$end_date,$kode_multipayment);
		$data["start_date"] = $start_date;
		$data["end_date"] = $end_date;
		$data["nama_multipayment"] = str_replace("%20"," ",$nama_multipayment);
		$this->load->view('laporan/all_transaction',$data);
    }
    
    public function pulsa_paket_data($kode,$table_name)
	{
			$data['ppd'] = $this->model_laporan->get_pulsa_paket_data($kode,$table_name);
	  	$this->load->view('laporan/pulsa_or_paket_data',$data);
    }
	
	public function bpjs($kode_bpjs)
	{
		$data['bpjs'] = $this->model_laporan->get_bpjs_by_code($kode_bpjs);
	  	$this->load->view('laporan/bpjs',$data);
    }

    public function tagihan_listrik($kode_tagihan)
	{
		$data['tagihan_listrik'] = $this->model_laporan->get_tagihan_by_code($kode_tagihan);
	  	$this->load->view('laporan/tagihan_listrik',$data);
    }

      public function token_listrik($kode_token)
	{
		$data['token_listrik'] = $this->model_laporan->get_token_by_code($kode_token);
	  	$this->load->view('laporan/token_listrik',$data);
    }

      public function air_pdam($kode_air)
	{
		$data['air_pdam'] = $this->model_laporan->get_air_by_code($kode_air);
	  	$this->load->view('laporan/air_pdam',$data);
    }
    
}
