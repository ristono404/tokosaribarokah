<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class paket_data extends CI_Controller {
    
    function __construct(){
		parent::__construct();
		$this->load->model('model_paket_data');
		$this->load->model('model_konsumen');
		$this->load->library('session');
	}

	public function index()
	{
		$data['js'] = "master/paket_data.js";
		$data['menu1'] = "Master";
		$data['menu2'] = "paket data";
		 $data['data_konsumen'] = $this->model_konsumen->get_data();
        $this->load->view('template/header',$data);
        $this->load->view('master/paket_data/index');
	    $this->load->view('template/footer',$data);
    }
    
    function get_data(){
		$data['data'] = $this->model_paket_data->get_data();
		$this->output->set_output(json_encode($data));
	}
	public function get_data_by_code($kode_paket_data){
		$data['data'] = $this->model_paket_data->get_data_by_code($kode_paket_data);
		$this->output->set_output(json_encode($data));
	}

	function save(){
		$data = $this->input->post('array_data');
		// var_dump($data);
		// die;
		$data = $this->model_paket_data->save($data);
		$this->output->set_output(json_encode($data));
	}
	function delete($kode_paket_data)
	{
		$data = $this->model_paket_data->delete($kode_paket_data);
		redirect('paket_data');
	}

    
}
