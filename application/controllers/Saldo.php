<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Saldo extends CI_Controller {
    
    function __construct(){
		parent::__construct();
		$this->load->model('model_saldo');
		$this->load->model('model_konsumen');
		$this->load->library('session');
	}

	public function index()
	{
		$data['js'] = "master/saldo.js";
		$data['menu1'] = "Master";
		$data['menu2'] = "Saldo";
        $this->load->view('template/header',$data);
        $this->load->view('master/saldo/index');
	    $this->load->view('template/footer',$data);
    }
    
    function get_data(){
		$data['data'] = $this->model_saldo->get_data();
		$this->output->set_output(json_encode($data));
	}
	public function get_data_by_code($kode_saldo){
		$data['data'] = $this->model_saldo->get_data_by_code($kode_saldo);
		$this->output->set_output(json_encode($data));
	}

	function save(){
		$data = $this->input->post('array_data');
		// var_dump($data);
		// die;
		$data = $this->model_saldo->save($data);
		$this->output->set_output(json_encode($data));
	}
	function delete($kode_saldo)
	{
		$data = $this->model_saldo->delete($kode_saldo);
		redirect('pulsa');
	}

    
}
