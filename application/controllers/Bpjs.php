<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bpjs extends CI_Controller {
    
    function __construct(){
		parent::__construct();
		$this->load->model('model_bpjs');
		$this->load->model('model_konsumen');
		$this->load->library('session');
	}

	public function index()
	{
		$data['js'] = "master/bpjs.js";
		$data['menu1'] = "Master";
		$data['menu2'] = "BPJS";
		$data['data_konsumen'] = $this->model_konsumen->get_data();
        $this->load->view('template/header',$data);
        $this->load->view('master/bpjs/index');
	    $this->load->view('template/footer',$data);
    }
    
    function get_data(){
		$data['data'] = $this->model_bpjs->get_data();
		$this->output->set_output(json_encode($data));
	}
	public function get_data_by_code($kode_bpjs){
		$data['data'] = $this->model_bpjs->get_data_by_code($kode_bpjs);
		$this->output->set_output(json_encode($data));
	}

	function save(){
		$data = $this->input->post('array_data');
		// var_dump($data);
		// die;
		$data = $this->model_bpjs->save($data);
		$this->output->set_output(json_encode($data));
	}
	function delete($kode_bpjs)
	{
		$data = $this->model_bpjs->delete($kode_bpjs);
		redirect('bpjs');
	}

    
}
