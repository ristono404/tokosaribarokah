<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Air_Pdam extends CI_Controller {

    function __construct(){
		parent::__construct();
		$this->load->model('model_air_pdam');
		$this->load->model('model_konsumen');
		$this->load->library('session');
		chek_session_admin();
	}
	public function index()
	{
		$data['js'] = "master/air_pdam.js";
		$data['menu1'] = "Master";
		$data['menu2'] = "PDAM";
		$data['data_konsumen'] = $this->model_konsumen->get_data();
        $this->load->view('template/header',$data);
        $this->load->view('air_pdam');
		$this->load->view('template/footer',$data);
	}

	function get_data(){
		
		$data['data'] = $this->model_air_pdam->get_data();
		$this->output->set_output(json_encode($data));
	}

	public function get_data_by_code($kode_pulsa){
		$data['data'] = $this->model_air_pdam->get_data_by_code($kode_pulsa);
		$this->output->set_output(json_encode($data));
	}

	public function save(){
		$data = $this->input->post('array_data');
		$data = $this->model_air_pdam->save($data);
		$this->output->set_output(json_encode($data));
	}

	public function update_data(){
        $kode_pdam = $this->input->post('kode_pdam');
		$data = $this->input->post('array_data');
		$data = $this->model_air_pdam->update_data($data,$kode_pdam);
		$this->output->set_output(json_encode($data));
	}

	public function delete_data($kode_pdam){
		$data = $this->model_air_pdam->delete_data($kode_pdam);
		if ($data) {
			redirect('/Air_Pdam');
		}
	}
}
