<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Token_listrik extends CI_Controller {
    
    function __construct(){
		parent::__construct();
		$this->load->model('model_token_listrik');
		$this->load->model('model_konsumen');
		$this->load->library('session');
	}

	public function index()
	{
		$data['js'] = "master/token_listrik.js";
		$data['menu1'] = "Master";
		$data['menu2'] = "Token listrik";
		$data['data_konsumen'] = $this->model_konsumen->get_data();
        $this->load->view('template/header',$data);
        $this->load->view('master/token_listrik/index');
	    $this->load->view('template/footer',$data);
    }
    
    function get_data(){
		$data['data'] = $this->model_token_listrik->get_data();
		$this->output->set_output(json_encode($data));
	}
	public function get_data_by_code($kode_token_listrik){
		$data['data'] = $this->model_token_listrik->get_data_by_code($kode_token_listrik);
		$this->output->set_output(json_encode($data));
	}

	function save(){
		$data = $this->input->post('array_data');
		// var_dump($data);
		// die;
		$data = $this->model_token_listrik->save($data);
		$this->output->set_output(json_encode($data));
	}
	function delete($kode_token_listrik)
	{
		$data = $this->model_token_listrik->delete($kode_token_listrik);
		redirect('token_listrik');
	}

    
}
