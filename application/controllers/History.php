<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History extends CI_Controller {
    
    function __construct(){ 
		parent::__construct();
		$this->load->model('model_history');
		$this->load->library('session');
	}

	public function index()
	{
		$data['js'] = "history.js";
		$data['menu1'] = "Laporan";
		$data['menu2'] = "Transaksi";
        $this->load->view('template/header',$data);
        $this->load->view('history');
	    $this->load->view('template/footer',$data);
    }

     function get_data(){
		$data['data'] = $this->model_history->get_data();
		$this->output->set_output(json_encode($data));
	}

	public function get_data_by_code($kode_transaksi){
		$data['data'] = $this->model_history->get_data_by_code($kode_transaksi);
		$this->output->set_output(json_encode($data));
	}
	function get_data_by_date(){
		$start_date = $this->input->post('start_date');
		$end_date =  $this->input->post('end_date');
		$kode_multipayment = $this->input->post('kode_multipayment');
		$data['data'] = $start_date == null ? $this->model_history->get_data($kode_multipayment) : $this->model_history->get_data_by_date($start_date,$end_date,$kode_multipayment);
		$this->output->set_output(json_encode($data));
	}

	function get_master_multipayment(){
		$data['data'] = $this->model_history->get_master_multipayment();
		$this->output->set_output(json_encode($data));
	}
    

    
}
