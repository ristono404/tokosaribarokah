<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('model_login');

	}

	public function index()
	{
		$this->load->view('view_login');
	}

	public function login_admin(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$data = $this->model_login->login($username,$password);
		if ($data) {
			redirect('/Home');
		}else{
			redirect('/Login');
		}
	}

	public function logout(){
		$this->session->unset_userdata('logged_in_multipayment');
		redirect('/Login');
	}

}
