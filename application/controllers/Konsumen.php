<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class konsumen extends CI_Controller {
    
    function __construct(){
		parent::__construct();
		$this->load->model('model_konsumen');
		$this->load->library('session');
	}

	public function index()
	{
		$data['js'] = "master/konsumen.js";
		$data['menu1'] = "Master";
		$data['menu2'] = "konsumen";
        $this->load->view('template/header',$data);
        $this->load->view('master/konsumen/index');
	    $this->load->view('template/footer',$data);
    }
    
    function get_data(){
		$data['data'] = $this->model_konsumen->get_data();
		$this->output->set_output(json_encode($data));
	}
	public function get_data_by_code($kode_konsumen){
		$data['data'] = $this->model_konsumen->get_data_by_code($kode_konsumen);
		$this->output->set_output(json_encode($data));
	}

	function save(){
		$data = $this->input->post('array_data');
		// var_dump($data);
		// die;
		$data = $this->model_konsumen->save($data);
		$this->output->set_output(json_encode($data));
	}
	function delete($kode_konsumen)
	{
		$data = $this->model_konsumen->delete($kode_konsumen);
		redirect('konsumen');
	}

    
}
