<div class="row">

    <div class="col-md-12">
        <div id="datatable" style="display:block">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">DATA SALDO</strong>
                    <button class="btn btn-info btn-sm pull-right" id="btn-add">

                        Add Saldo
                    </button>
                </div>
                <div class="card-body">
                    <table id="dt-saldo" class="display table table-bordered table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th>Kode pulsa</th>
                                <th>Jumlah saldo</th>
                                <th>Tanggal Masuk</th>
                                <th>Aksi</th>
                                <!--
                                <th><center><button class="btn btn-info btn-sm" id="btn_add">
                                    <span class="btn-label">
                                        <i class="material-icons">add</i>
                                    </span>
                                    Add Data
                                </button></center></th> -->
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>


        <div id="form" style="display:none">
            <div class="card">
                <div class="card-header">
                    <strong class="form-title">Tambah data Saldo</strong>
                </div>
                <div class="card-body card-block">
                    <form class="form-horizontal" id="form-saldo">
                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group bmd-form-group">
                                    <label class=" form-control-label">Kode Saldo</label>
                                    <div class="input-group ">
                                        <div class="input-group-addon"><i class="fa fa-code"></i></div>
                                        <input class="form-control txt-input" name="kode_saldo" readonly value="**Generated by System**">
                                    </div>
                                </div>
                            </div>

                           
                            </div>
                            <div class="row">
                                    <div class="col-sm-4">
                                    <div class="form-group bmd-form-group">
                                        <label class=" form-control-label">Jumlah Saldo</label>
                                        <div class="input-group ">
                                            <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                            <input name="jumlah_saldo" class="form-control txt-input">
                                        </div>
                                        <small class="form-text text-muted">ex. 999-99-9999</small>
                                    </div>
                                </div>

                               
                            </div>

                            <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group bmd-form-group">
                                <label class=" form-control-label">Tanggal Transaksi</label>
                                <div class="input-group ">
                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <input name="tgl_transaksi" type="date" class="form-control txt-input">
                                </div>
                                <small class="form-text text-muted">ex. 999-99-9999</small>
                            </div>
                        </div>
                            
                            </div>
                            <div class="col-md-12">
                                <button type="button" class="btn btn-sm btn-primary" id="btn-save">SIMPAN</button>
                                <button type="button" class="btn btn-default btn-sm" id="btn-clear">CLEAR</button>
                                <button type="button" class="btn btn-sm btn-danger pull-right" id="btn-cancel">KEMBALI</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

        </div>


    </div>



