<div class="row">

    <div class="col-md-12">
        <div id="datatable" style="display:block">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">DATA TOKEN LISTRIK</strong>
                    <button class="btn btn-info btn-sm pull-right" id="btn-add">
                        Add token listrik
                    </button>
                </div>
                <div class="card-body">
                    <table id="dt-token-listrik" class="display table table-bordered table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th>Kode token</th>
                                <th>Nama Konsumen</th>
                                <th>No Token</th>
                                <th>Tanggal Transaksi</th>
                                <th>no seri</th>
                                <th>Harga</th>
                                <th>kwh</th>
                                <th>Aksi</th>
                                <!--
                                <th><center><button class="btn btn-info btn-sm" id="btn_add">
                                    <span class="btn-label">
                                        <i class="material-icons">add</i>
                                    </span>
                                    Add Data
                                </button></center></th> -->
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>


        <div id="form" style="display:none">
            <div class="card">
                <div class="card-header">
                    <strong class="form-title">Tambah data token</strong>
                </div>
                <div class="card-body card-block">
                    <form class="form-horizontal" id="form-token-listrik">
                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group bmd-form-group">
                                    <label class=" form-control-label">Kode token</label>
                                    <div class="input-group ">
                                        <div class="input-group-addon"><i class="fa fa-code"></i></div>
                                        <input class="form-control txt-input" name="kode_token" readonly value="**Generated by System**">
                                    </div>
                                </div>
                            </div> 
                       </div>
                       <div class="row">
                        <div class="col-sm-4">
                                <div class="form-group bmd-form-group">
                                    <label class=" form-control-label">Nama Konsumen</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                        <select  id="id-slc-konsumen" name="id_konsumen">
                                          <option  value="" data-display="Child">Pilih konsumen</option>
                                          
                                          <?php 
                                          foreach ($data_konsumen as $key) {    
                                           ?>
                                           <option class="form-control" value="<?php  echo $key->kode_konsumen ?>"><?php   echo $key->kode_konsumen." - ".$key->nm_konsumen ?></option>
                                           <?php } ?>
                                       </select>
                                        </div>
                                   
                               </div>
                           </div> 

                         <div class="col-sm-4">
                            <div class="form-group bmd-form-group">
                                <label class=" form-control-label">No Seri</label>
                                <div class="input-group ">
                                    <div class="input-group-addon"><i class="fa fa-pencil"></i></div>
                                    <input name="no_seri" class="form-control txt-input">
                                </div>
                                <small class="form-text text-muted">ex. sdsd2324234</small>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                       <div class="col-sm-4">
                        <div class="form-group bmd-form-group">
                            <label class=" form-control-label">Harga</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                <input name="harga" class="form-control txt-input">
                            </div>
                            <small class="form-text text-muted">ex. 50000</small>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group bmd-form-group">
                            <label class=" form-control-label">No Token</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-pencil"></i></div>
                                <input name="no_konsumen" class="form-control txt-input">
                            </div>
                            <small class="form-text text-muted">ex. 0343432</small>
                        </div>
                    </div>
                </div>

                <div class="row">
                   <div class="col-sm-4">
                    <div class="form-group bmd-form-group">
                        <label class=" form-control-label">KWH</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-pencil"></i></div>
                            <input name="kwh" class="form-control txt-input">
                        </div>
                        <small class="form-text text-muted">ex. 40</small>
                    </div>
                </div>
                 <div class="col-sm-4">
                    <div class="form-group bmd-form-group">
                        <label class=" form-control-label">Tanggal Transaksi</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            <input name="tgl_transaksi" type="date" class="form-control txt-input">
                        </div>
                        <small class="form-text text-muted">ex. 20/04/2019</small>
                    </div>
                </div>
               
            </div>
          

            <div class="col-md-12">
                <button type="button" class="btn btn-sm btn-primary" id="btn-save">SIMPAN</button>
                <button type="button" class="btn btn-default btn-sm" id="btn-clear">CLEAR</button>
                <button type="button" class="btn btn-sm btn-danger pull-right" id="btn-cancel">KEMBALI</button>
            </div>
        </form>
    </div>

</div>
</div>

</div>


</div>



