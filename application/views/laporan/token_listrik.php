<?php
$pdf = new FPDF('P','mm',array(100,150)); //L For Landscape / P For Portrait
$pdf->AddPage();
 date_default_timezone_set("Asia/Bangkok");
//Menambahkan Gambar
//$pdf->Image('../foto/logo.png',10,10,-175);
  $pdf->Image('assets/img/logo-pot.png',9,7,-250);
  $pdf->SetFont('Arial','B',20);
  $pdf->setFont('ARIAL','B',10);
  $pdf->Text(30,12,'PO. SARI BAROKAH');
  $pdf->setFont('ARIAL','',8);
  $pdf->Text(30,17,'Jl. Kranggan No.15');
  $pdf->Text(30,20,'Bekasi - INDONESIA');
  $pdf->Text(30,23,'Telp : +62 21 5266');
  $pdf->Text(30,26,'Fax +62 21 53662323, 5840184');

  $pdf->setFont('ARIAL','B',10);
  $pdf->Text(115,17,'LAPORAN TRANSAKSI');
  //horizontal
  $pdf->SetLineWidth(0.5);
  $pdf->Line(10,30,80,30);

  $pdf->SetFont('Arial','B',9);
  $pdf->Text(15,37,' STRUK PEMBELIAN TOKEN LISTRIK',1,4,'L');
  $pdf->ln(30);



  //vertikel
  $pdf->setFont('Arial','',9);
  $pdf->SetFillColor(224,224,224);
  $pdf->ln(3);
  $pdf->cell(3);
  $pdf->Cell(23,6,'TANGGAL',0,0,'L');
  $pdf->Cell(35,6,': '.tgl_indo($token_listrik->tgl_transaksi),0,1,'L');

  $pdf->cell(3);
  $pdf->Cell(23,6,'NO SERI',0,0,'L');
 $pdf->Cell(35,6,': '.$token_listrik->no_seri,0,1,'L');


   $pdf->cell(3);
  $pdf->Cell(23,6,'KWH',0,0,'L');
  $pdf->Cell(35,6,': '.$token_listrik->kwh,0,1,'L');

  $pdf->cell(3);
  $pdf->Cell(23,6,'NO TOKEN',0,0,'L');
  $pdf->Cell(35,6,': '.$token_listrik->no_konsumen,0,1,'L');

  $pdf->cell(3);
  $pdf->Cell(23,6,'HARGA',0,0,'L');
  $pdf->Cell(35,6,': Rp. '.number_format($token_listrik->harga,2,',','.'),0,1,'L');
 
  $pdf->ln(8);

  $pdf->Cell(3);
  $pdf->setFont('Arial','I',10);
  $pdf->Cell(58,3,'SIMPAN TANDA TERIMA INI',0,1,'C');

  $pdf->Cell(3);  $pdf->Cell(58,3,'SEBAGAI BUKTI TRANSAKSI',0,1,'C');

  $pdf->Cell(3);
  $pdf->Cell(58,3,'TERIMAKASIH',0,1,'C');

  $pdf->Output();
	
?>