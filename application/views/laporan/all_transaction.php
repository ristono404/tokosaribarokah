<?php
$pdf = new FPDF('P','mm','A3'); //L For Landscape / P For Portrait
$pdf->AddPage();
 date_default_timezone_set("Asia/Bangkok");
//Menambahkan Gambar
//$pdf->Image('../foto/logo.png',10,10,-175);
  $pdf->Image('assets/img/logo-pot.png',3,3,-175);
  $pdf->SetFont('Arial','B',20);
  $pdf->setFont('ARIAL','B',10);
  $pdf->Text(30,12,'PO. SARI BAROKAH');
  $pdf->setFont('ARIAL','',8);
  $pdf->Text(30,17,'Jl. Kranggan No.15, Bekasi - INDONESIA');
  $pdf->setFont('ARIAL','',8);
  $pdf->Text(30,20,'Telp : +62 21 5266 - Fax +62 21 53662323, 5840184');

  $pdf->setFont('ARIAL','B',10);
  $pdf->Text(115,17,'LAPORAN TRANSAKSI');
  //horizontal
  $pdf->SetLineWidth(0.5);
  $pdf->Line(12,30,285,30);

  $pdf->SetFont('Arial','B',9);
  $pdf->Text(20,37,' Laporan Transaksi Per Periode',1,4,'L');
  $pdf->ln(30);
  $pdf->SetFont('Arial','B',8);
 
  $pdf->Cell(10);
  $pdf->setFont('Arial','B',8);
  $pdf->cell(20,4,' Start Date   :',0,0,'L');
  $pdf->setFont('Arial','',8);
  $pdf->cell(50,4,$start_date==null ? '-' : tgl_indo($start_date),0,1,'L');
 
  $pdf->setFont('ARIAL','B',8);
  $pdf->Text(230,50,'Jenis Transaksi  : '.($nama_multipayment==null?'ALL':$nama_multipayment));

  $pdf->Cell(10);
  $pdf->setFont('Arial','B',8);
  $pdf->cell(20,4,' End Date   :',0,0,'L');
  $pdf->setFont('Arial','',8);
  $pdf->cell(50,4,$end_date==null ? '-' : tgl_indo($end_date),0,1,'L');


  //vertikel
  $pdf->setFont('Arial','B',9);
  $pdf->SetFillColor(224,224,224);
  $pdf->ln(5);
  $pdf->cell(10);
  $pdf->Cell(8,6,'No',0,0,'C',1);
  $pdf->Cell(25,6,'Kode ',0,0,'C',1);
  $pdf->Cell(40,6,'Jenis',0,0,'C',1);
  $pdf->Cell(35,6,'Tanggal',0,0,'C',1);
  $pdf->Cell(50,6,'Add',0,0,'C',1);
  $pdf->Cell(50,6,'Red',0,0,'C',1);
  $pdf->Cell(50,6,'Total Saldo',0,1,'C',1);
 
  $pdf->setFont('Arial','',8);
  
  $no = 1;
  $total = 0;
  foreach ($all_transaction as $at) {
  //   $gaji_bruto = str_replace(',', '.', $g->GAJI_BRUTO);
  //   $gaji_hari_lebih = str_replace(',', '.', $g->GAJI_HARI_LEBIH);
  //   $gaji_lembur = str_replace(',', '.', $g->GAJI_LEMBUR);
  //   $pph = str_replace(',', '.', $g->PPH);
  //   $pot_alfa = str_replace(',', '.', $g->POT_ALFA);
  //   $gaji_total = str_replace(',', '.', $g->GAJI_TOTAL);
  //   $total += $gaji_total;
    
     $pdf->cell(10);
      $pdf->Cell(8,6,$no,0,0,'C');
      $pdf->Cell(25,6,$at->kode_transaksi,0,0,'C');
      $pdf->Cell(40,6,$at->jenis_transaksi,0,0,'C');
      $pdf->Cell(35,6,$at->tgl_transaksi,0,0,'C');
      $pdf->Cell(50,6,$at->add != 0 ? 'Rp. '.number_format($at->add,2,',','.') : 0,0,0,'C');
      $pdf->Cell(50,6,$at->red != 0 ? 'Rp. '.number_format($at->red,2,',','.') : 0,0,0,'C');
      $pdf->Cell(50,6,'Rp. '.number_format($at->total,2,',','.'),0,1,'C');
      $no++;
  }
  $total = str_replace(',', '.', $total);
  $pdf->Cell(10);
  $pdf->setFont('Arial','',8);
  $pdf->Cell(220,6,'Saldo Akhir',0,0,'R',1);
  $pdf->setFont('Arial','B',8);
  $pdf->Cell(40,6,'Rp. '.number_format(get_last_saldo(),2,',','.'),0,1,'L',1);
  $pdf->cell(10);
  $pdf->Cell(260,0.12,'',1,1,'C');
  $pdf->setFont('Arial','B',8);
  $pdf->cell(10);
  $pdf->Cell(125,7,'Printing Date/time '.tgl_indo(date('Y-m-d')).'/'.date('H:i:s'),0,0,'L');
  $pdf->Cell(125,7,'Pimpinan',0,1,'R');
  $pdf->ln(5);
  $pdf->setFont('Arial','',10);
  $pdf->Cell(268,7,'Sari Afrilyah',0,1,'R');

  $pdf->Output();
	
?>