 <div class="content">
   <div class="animated fadeIn">
    <!-- Widgets  -->
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <a href="<?php echo base_url(); ?>Pulsa">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-five">
                        <div class="stat-icon dib flat-color-1">
                            <i class="fa fa-mobile"></i>
                        </div>
                        <div class="stat-content">
                            <div class="text-left dib">
                                <div class="stat-text"></div>
                                <div class="stat-heading">PULSA</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </a>
        </div>

        <div class="col-lg-6 col-md-6">
            <a href="<?php echo base_url(); ?>Bpjs">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-five">
                        <div class="stat-icon dib flat-color-2">
                            <i class="fa fa-credit-card"></i>
                        </div>
                        <div class="stat-content">
                            <div class="text-left dib">
                                <div class="stat-text"></div>
                                <div class="stat-heading">BPJS</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </a>
        </div>

        <div class="col-lg-6 col-md-6">
            <a href="<?php echo base_url(); ?>Token_listrik">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-five">
                        <div class="stat-icon dib flat-color-3">
                            <i class="fa fa-bolt"></i>
                        </div>
                        <div class="stat-content">
                            <div class="text-left dib">
                                <div class="stat-text"></div>
                                <div class="stat-heading">TOKEN LISTRIK</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        </div>


        <div class="col-lg-6 col-md-6">
            <a href="<?php echo base_url(); ?>Paket_data">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-five">
                        <div class="stat-icon dib flat-color-4">
                            <i class="fa fa-mobile"></i>
                        </div>
                        <div class="stat-content">
                            <div class="text-left dib">
                                <div class="stat-text"></div>
                                <div class="stat-heading">PAKET DATA</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </a>
        </div>

         <div class="col-lg-6 col-md-6">
            <a href="<?php echo base_url(); ?>Tagihan_Listrik">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-five">
                        <div class="stat-icon dib flat-color-4">
                            <i class="fa fa-bolt"></i>
                        </div>
                        <div class="stat-content">
                            <div class="text-left dib">
                                <div class="stat-text"></div>
                                <div class="stat-heading">TAGIHAN LISTRIK</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </a>
        </div>

           <div class="col-lg-6 col-md-6">
            <a href="<?php echo base_url(); ?>Air_Pdam">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-five">
                        <div class="stat-icon dib flat-color-1">
                            <i class="fa fa-tint"></i>
                        </div>
                        <div class="stat-content">
                            <div class="text-left dib">
                                <div class="stat-text"></div>
                                <div class="stat-heading">AIR PDAM</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </a>
        </div>


           <div class="col-lg-6 col-md-6">
            <a href="<?php echo base_url(); ?>History">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-five">
                        <div class="stat-icon dib flat-color-7">
                            <i class="fa fa-file    "></i>
                        </div>
                        <div class="stat-content">
                            <div class="text-left dib">
                                <div class="stat-text"></div>
                                <div class="stat-heading">LAPORAN</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </a>
        </div>
    </div>
    <!-- /Widgets -->
    <!--  Traffic  -->

    <!--  /Traffic -->
    <div class="clearfix"></div>
    <!-- Orders -->

    <!-- /.orders -->
    <!-- To Do and Live Chat -->



</div>
</div>
</div>
<!-- /#add-category -->
</div>
</div>