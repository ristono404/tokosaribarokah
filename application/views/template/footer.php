 </div>
 <!-- /.content -->

</div>
<div class="clearfix"></div>
 <!-- Footer -->
 <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; 2018 Novi AP
                    </div>
                    <div class="col-sm-6 text-right">
                    </div>
                </div>
            </div>
        </footer>
<!-- /.site-footer -->
</div>
<!-- /#right-panel -->


<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
<script src="<?php echo base_url(); ?>assets/js/numeral.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/buttons.colVis.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/datatables-init.js"></script>
<script src="<?php echo base_url(); ?>assets/js/common.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datetimepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datetimepicker/datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datetimepicker/datepickers.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datetimepicker/date.format.js"></script>


<!-- Scripts -->
<script>
    numeral.register('locale', 'id', {
    delimiters: {
        thousands: '.',
        decimal: ','
    },
    abbreviations: {
        thousand: 'k',
        million: 'm',
        billion: 'b',
        trillion: 't'
    },
    ordinal : function (number) {
        return number === 1 ? 'er' : 'ème';
    },
    currency: {
        symbol: '$'
    }
});
// switch between locales
numeral.locale('id');
</script>

<script src="<?php echo base_url(); ?>assets/js/<?=$js?>"></script>

</body>
</html>
