<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sari Barokah</title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/img/QRAUqs9.png">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/QRAUqs9.png">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/cs-skin-elastic.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/dataTables.bootstrap.min.css">
    
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>
<style>
    body {
   
    background-color: #eaebf0;
}
</style>
<body>
    <!-- Left Panel -->
    
    <input type="hidden" name="" id="base_url" value="<?=base_url()?>">
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="">
                        <a href="<?=base_url()?>"><i class="menu-icon fa fa-home"></i>Dashboard </a>
                    </li>
                    <li class="menu-title">UI elements</li><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Data Master</a>
                        <ul class="sub-menu children dropdown-menu">                           
                            
                             <li><i class="fa fa-bolt"></i><a href="<?php echo base_url(); ?>Konsumen">KONSUMEN</a></li>  
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-exchange"></i>Data Transaksi</a>
                        <ul class="sub-menu children dropdown-menu">                       
                                <li><i class="fa fa-mobile"></i><a href="<?php echo base_url(); ?>Saldo"> SALDO</a></li>     
                            <li><i class="fa fa-usd"></i><a href="<?php echo base_url(); ?>Pulsa"> PULSA</a></li>
                            <li><i class="fa fa-credit-card"></i><a href="<?php echo base_url(); ?>bpjs"> BPJS</a></li>
                            <li><i class="fa fa-bolt"></i><a href="<?php echo base_url(); ?>token_listrik">TOKEN LISTRIK</a></li>

                            <li><i class="fa fa-mobile"></i><a href="<?php echo base_url(); ?>paket_data">PAKET DATA</a></li>
                            <li><i class="fa fa-tint"></i><a href="<?php echo base_url(); ?>Air_Pdam"> AIR PDAM</a></li>
                            <li><i class="fa fa-bolt"></i><a href="<?php echo base_url(); ?>Tagihan_Listrik">TAGIHAN LISTRIK</a></li>             </ul>
                    </li>
                   
                    <li>
                        <a href="<?php echo base_url(); ?>History"> <i class="menu-icon fa fa-bar-chart-o"></i>LAPORAN </a>
                    </li>
                   
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>
    <!-- /#left-panel -->
    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                    <a class="navbar-brand" href="./"><img src="<?php echo base_url() ?>assets/img/logo.png" alt="Logo"></a>
                    <a class="navbar-brand hidden" href="./"><img src="<?php echo base_url() ?>assets/img/logo2.png" alt="Logo"></a>
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>
            <div class="top-right">
                <div class="header-menu">
                   

                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="user-avatar rounded-circle" src="<?php echo base_url(); ?>assets/img/admin.jpg" alt="User Avatar">
                        </a>

                        <div class="user-menu dropdown-menu">
                         <!--    <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a> -->
                            <a class="nav-link" href="<?php echo base_url(); ?>Login/logout"><i class="fa fa-power -off"></i>Logout</a>
                        </div>
                    </div>

                </div>
            </div>
        </header>
        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1><?=$menu1?></h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#"><?=$menu1?></a></li>
                                    <li><a href="#"><?=$menu2?></a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="content">
            <!-- Animated -->
            <div class="animated fadeIn">
