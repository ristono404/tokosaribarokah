<?php
class model_laporan extends ci_model{

   public function __construct() 
   {
     parent::__construct(); 
     $this->load->database();
 }
 function get_all_transaction($kode_multipayment=null)
 {
    $query= "SELECT ttt.kode_transaksi
                   ,DATE_FORMAT(tgl_transaksi,'%d-%c-%Y')tgl_transaksi
                   , ttt.add
                   , ttt.red 
                   , ttt.total
                   , tmm.nama_multipayment jenis_transaksi 
            FROM `tbl_t_transaksi` ttt, tbl_m_multipayment tmm 
            WHERE ttt.kode_multipayment = tmm.kode_multipayment 
                  AND ttt.kode_multipayment ".($kode_multipayment==null ? "IS NOT NULL" : " ='".$kode_multipayment."'"."")."";
    return $this->db->query($query)->result();
}

function get_pulsa_paket_data($kode,$table_name)
{
    $key = $table_name == "pulsa" ? "kode_pulsa" : "kode_paket_data";
    $table_name = $table_name == "pulsa" ? "tbl_m_pulsa" : "tbl_m_paket_data";
    $query= "SELECT *
    FROM $table_name
    WHERE $key = '".$kode."'";
    return $this->db->query($query)->row();
}
function get_bpjs_by_code($kode_bpjs)
{

    $query= "SELECT bpj.*,
    kon.nm_konsumen
    FROM tbl_m_bpjs bpj
    INNER JOIN tbl_m_konsumen kon ON kon.kode_konsumen=bpj.id_konsumen
    WHERE bpj.kode_bpjs = '".$kode_bpjs."'";
    return $this->db->query($query)->row();
}
function get_data_by_code($kode_transaksi)
{
    $query= "SELECT tmm.table_name, tmm.key, ttt.kode_submultipayment,tmm.nama_multipayment FROM `tbl_t_transaksi` ttt, tbl_m_multipayment tmm where ttt.kode_multipayment = tmm.kode_multipayment and ttt.kode_transaksi = '".$kode_transaksi."' ";
    $data= $this->db->query($query)->row();
    $table_name = $data->table_name;
    $key = $data->key;
    $kode_submultipayment = $data->kode_submultipayment;
    $nama_multipayment = $data->nama_multipayment;
    $query = "SELECT a.*, DATE_FORMAT(a.tgl_transaksi,'%d-%c-%Y')tgl_transaksi, b.nm_konsumen,b.kode_konsumen,'".$nama_multipayment."' as nm_multipayment, '".$kode_transaksi."' as kode_transaksi  FROM ".$table_name." a, tbl_m_konsumen b  WHERE ".$key." = '".$kode_submultipayment."' and a.id_konsumen = b.kode_konsumen";
    $data= $this->db->query($query)->row();
    
    return $data;
}

function get_data_by_date($start_date,$end_date,$kode_multipayment=null)
{   
    $query= "SELECT ttt.kode_transaksi,
                    DATE_FORMAT(tgl_transaksi,'%d-%c-%Y')tgl_transaksi,
                     ttt.add, ttt.red, 
                     ttt.total,
                     tmm.nama_multipayment jenis_transaksi 
            FROM `tbl_t_transaksi` ttt, tbl_m_multipayment tmm 
            where ttt.kode_multipayment = tmm.kode_multipayment 
                AND ttt.kode_multipayment ".($kode_multipayment==null ? "IS NOT NULL" : " ='".$kode_multipayment."'"."")."
                AND tgl_transaksi between STR_TO_DATE('".$start_date."', '%Y-%m-%d') 
                AND STR_TO_DATE('".$end_date."', '%Y-%m-%d') ";

    $data= $this->db->query($query)->result();  
    return $data;
}

function get_tagihan_by_code($kode_tagihan)
{
    $query= "SELECT kode_tagihan,no_meter,DATE_FORMAT(tgl_transaksi,'%Y-%m-%d') as tgl_transaksi,id_konsumen,DATE_FORMAT(periode,'%Y-%m-%d') as periode,harga,kwh FROM tbl_m_tagihan_listrik WHERE kode_tagihan ='".$kode_tagihan."'";
    $data= $this->db->query($query)->row();
    
    return $data;
}


function get_token_by_code($kode_token)
{
    $query= "SELECT * FROM tbl_m_token_listrik WHERE kode_token='".$kode_token."'";
    $data= $this->db->query($query)->row();
    
    return $data;
}

function get_air_by_code($kode_pdam)
{
  $query= "SELECT kode_pdam,no_pdam,DATE_FORMAT(tgl_transaksi,'%Y-%m-%d') as tgl_transaksi,id_konsumen,DATE_FORMAT(periode,'%Y-%m-%d') as periode,harga FROM tbl_m_air_pdam WHERE kode_pdam ='".$kode_pdam."'";
  $data= $this->db->query($query)->row();

  return $data;
}



}