<?php
class model_saldo extends ci_model{

   public function __construct() 
   {
     parent::__construct(); 
     $this->load->database();
   }
 function get_data()
 {
    $query= "SELECT  tsd.kode_saldo
                    ,tsd.jumlah_saldo
                    ,DATE_FORMAT(trs.tgl_transaksi,'%d-%c-%Y')tgl_transaksi
            FROM tbl_t_saldo tsd
            INNER JOIN tbl_t_transaksi trs ON trs.kode_submultipayment=tsd.kode_saldo";
    return $this->db->query($query)->result();
  }
    function get_data_by_code($kode_saldo)
    {
        $query= "SELECT  tsd.kode_saldo
                        ,tsd.jumlah_saldo
                        ,trs.tgl_transaksi
                FROM tbl_t_saldo tsd
                INNER JOIN tbl_t_transaksi trs ON trs.kode_submultipayment=tsd.kode_saldo
                WHERE kode_saldo='".$kode_saldo."'";
        $data= $this->db->query($query)->row();
        
        return $data;
    }

    function save($data)
    {
    // $data = json_decode($data,true);
    $status_save =false;

    // var_dump($data);
    // die;

    try {
       
       $kode_saldo = $data[0]['kode_saldo'];

    // var_dump($package_code);
    // $saldo_update =  $data[0]['jumlah_saldo'];
    // $query = "SELECT jumlah_saldo FROM tbl_t_saldo WHERE kode_saldo='".$kode_saldo."'";

    // $jumlah_saldo =  $this->db->query($query)->row()->jumlah_saldo;
    // echo get_last_saldo()+($saldo_update-$jumlah_saldo);
    // die;

       if (\strpos($kode_saldo, '**') !== false) {
            //SAVE HEADER
        $query = "SELECT (substring(max(kode_saldo),3,3)*1) as kode FROM tbl_t_saldo ;";
        $count_data = $this->db->query($query)->row()->kode;
        $length_data = strlen($count_data);
        $kode_saldo = generate_code("SD",$length_data, $count_data);

        $data_array = array(
            'kode_saldo'=>  $kode_saldo,
            'jumlah_saldo'=>$data[0]['jumlah_saldo'],
        );
            //SAVE HEADER
        $this->db->insert('tbl_t_saldo', $data_array); 
            //SAVE DETAIL
        $query = "SELECT (substring(max(kode_transaksi),3,3)*1) as kode FROM tbl_t_transaksi ;";
       $count_data = $this->db->query($query)->row()->kode;
       $length_data = strlen($count_data);
       $kode_transaksi = generate_code("TR",$length_data, $count_data);
       $data_array = array(
        'KODE_TRANSAKSI'=>  $kode_transaksi,
        'KODE_MULTIPAYMENT' => 'MP001',
        'KODE_SUBMULTIPAYMENT' => $kode_saldo,
        'TGL_TRANSAKSI' =>date('Y-m-d',strtotime($data[0]['tgl_transaksi'] )),
        'ADD' => $data[0]['jumlah_saldo'],
        'RED' => 0,
        'TOTAL' => get_last_saldo()+$data[0]['jumlah_saldo']
    );
       $this->db->insert('tbl_t_transaksi', $data_array);

    }else{
        $data_array = array(
            'kode_saldo'=>  $kode_saldo,
            'jumlah_saldo'=>$data[0]['jumlah_saldo'],
        );

        $query = "SELECT jumlah_saldo FROM tbl_t_saldo WHERE kode_saldo='".$kode_saldo."'";

        $jumlah_saldo =  $this->db->query($query)->row()->jumlah_saldo;
        $saldo_update =  $data[0]['jumlah_saldo'];

        $this->db->where('kode_saldo',$kode_saldo);
        $this->db->update('tbl_t_saldo',$data_array);
       //SALDO_AKHIR + (CHANGE - ORI) = 
        $data_array = array(
            'TGL_TRANSAKSI' =>date('Y-m-d',strtotime($data[0]['tgl_transaksi'] )),
            'ADD' => $saldo_update
        );
           
            $this->db->query("call update_trans_saldo('".($kode_saldo)."', ".(($saldo_update)-($jumlah_saldo)).")");
    
            $this->db->where('kode_submultipayment',$kode_saldo);
            $this->db->update('tbl_t_transaksi',$data_array);

    }
      

} catch (Exception $e) {
      //$this->db->trans_rollback();
}
 return true;//$status_save;
}


function delete($kode_saldo)
{
    $this->db->where('kode_saldo',$kode_saldo);
    $this->db->delete('tbl_t_saldo');
    return true;
}
}