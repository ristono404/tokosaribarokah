<?php
class model_konsumen extends ci_model{
    
 public function __construct() 
 {
   parent::__construct(); 
   $this->load->database();
}
function get_data()
{
    $query= "SELECT * FROM tbl_m_konsumen";
    return $this->db->query($query)->result();
}
function get_data_by_code($kode_konsumen)
{
    $query= "SELECT * FROM tbl_m_konsumen WHERE kode_konsumen='".$kode_konsumen."'";
    $data= $this->db->query($query)->row();
    
    return $data;
}

function save($data)
{
    // $data = json_decode($data,true);
    $status_save =false;

    // var_dump($data);
    // die;

    try {
        // $this->db->trans_start(); 
        // $this->db->trans_strict(FALSE);
        // echo  count($data[0]['detail_data']);
        // die;
         $kode_konsumen = $data[0]['kode_konsumen'];
         
    // var_dump($package_code);
    // die;
        if (\strpos($kode_konsumen, '**') !== false) {
            //SAVE HEADER
            $query = "SELECT CONCAT('KN',RIGHT(CONCAT('000',CAST((SELECT COALESCE(MAX(RIGHT(kode_konsumen,3)),0) FROM tbl_m_konsumen ) AS UNSIGNED INTEGER)+1),3)) AS RESULT";
            $result = $this->db->query($query)->row();
            $kode_konsumen = $result->RESULT;
            $data_array = array(
                'kode_konsumen'=>  $kode_konsumen,
                'nm_konsumen' => $data[0]['nm_konsumen'],
                'no_telepon' => $data[0]['no_telepon']
            );
            //SAVE HEADER
            $this->db->insert('tbl_m_konsumen', $data_array);
            //SAVE DETAIL

        }else{
            $data_array = array(
                'kode_konsumen'=>  $kode_konsumen,
                'nm_konsumen' => $data[0]['nm_konsumen'],
                'no_telepon' => $data[0]['no_telepon']
            );

            $this->db->where('kode_konsumen',$kode_konsumen);
            $this->db->update('tbl_m_konsumen',$data_array);
            
        }

 } catch (Exception $e) {
      //$this->db->trans_rollback();
 }
 return true;//$status_save;
}


function delete($kode_konsumen)
{
    $this->db->where('kode_konsumen',$kode_konsumen);
    $this->db->delete('tbl_m_konsumen');
    return true;
}
 }