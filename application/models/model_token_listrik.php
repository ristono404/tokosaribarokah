<?php
class model_token_listrik extends ci_model{
    
 public function __construct() 
 {
   parent::__construct(); 
   $this->load->database();
}
function get_data()
{
    $query= "SELECT tml.*,DATE_FORMAT(tml.tgl_transaksi,'%d-%c-%Y')tgl_transaksi,tmk.nm_konsumen FROM tbl_m_token_listrik tml, tbl_m_konsumen tmk where tml.id_konsumen = tmk.kode_konsumen";
    return $this->db->query($query)->result();
}
function get_data_by_code($kode_token)
{
    $query= "SELECT * FROM tbl_m_token_listrik WHERE kode_token='".$kode_token."'";
    $data= $this->db->query($query)->row();
    
    return $data;
}

function save($data)
{
    // $data = json_decode($data,true);
    $status_save =false;

    // var_dump($data);
    // die;

    try {
        // $this->db->trans_start(); 
        // $this->db->trans_strict(FALSE);
        // echo  count($data[0]['detail_data']);
        // die;
     $kode_token = $data[0]['kode_token'];
     
    // var_dump($package_code);
    // die;

     if (\strpos($kode_token, '**') !== false) {
            //SAVE HEADER
        $query = "SELECT (substring(max(kode_token),3,3)*1) as kode FROM tbl_m_token_listrik ;";
        $count_data = $this->db->query($query)->row()->kode;
        $length_data = strlen($count_data);
        $kode_token = generate_code("TK",$length_data, $count_data);
        $data_array = array(
            'kode_token'=>  $kode_token,
            'id_konsumen' => $data[0]['id_konsumen'],
            'no_konsumen' => $data[0]['no_konsumen'],
            'tgl_transaksi' => $data[0]['tgl_transaksi'],
            'no_seri' => $data[0]['no_seri'],
            'harga' => $data[0]['harga'],
            'kwh' => $data[0]['kwh'],
        );
            //SAVE HEADER
        $this->db->insert('tbl_m_token_listrik', $data_array);
            //SAVE DETAIL
        $query = "SELECT (substring(max(kode_transaksi),3,3)*1) as kode FROM tbl_t_transaksi ;";
        $count_data = $this->db->query($query)->row()->kode;
        $length_data = strlen($count_data);
        $kode_transaksi = generate_code("TR",$length_data, $count_data);
        $data_array = array(
            'KODE_TRANSAKSI'=>  $kode_transaksi,
            'KODE_MULTIPAYMENT' => 'MP007',
            'KODE_SUBMULTIPAYMENT' => $kode_token,
            'TGL_TRANSAKSI' =>date('Y-m-d',strtotime($data[0]['tgl_transaksi'] )),
            'ADD' => 0,
            'RED' => $data[0]['harga'],
            'TOTAL' => get_last_saldo()-$data[0]['harga']
        );
        $this->db->insert('tbl_t_transaksi', $data_array);
            $data['kode_trans'] = $kode_token;
            $data['status'] = true;  
    }else{
        $data_array = array(
            'kode_token'=>  $kode_token,
            'id_konsumen' => $data[0]['id_konsumen'],
            'no_konsumen' => $data[0]['no_konsumen'],
            'tgl_transaksi' => $data[0]['tgl_transaksi'],
            'no_seri' => $data[0]['no_seri'],
            'harga' => $data[0]['harga'],
            'kwh' => $data[0]['kwh'],
        );
        
        $query = "SELECT harga FROM tbl_m_token_listrik WHERE kode_token ='".$kode_token."'";
        $jumlah_saldo =  $this->db->query($query)->row()->harga;
        $saldo_update =  $data[0]['harga'];

        $this->db->where('kode_token',$kode_token);
        $this->db->update('tbl_m_token_listrik',$data_array);

        $data_array = array(
            'TGL_TRANSAKSI' =>date('Y-m-d',strtotime($data[0]['tgl_transaksi'] )),
            'RED' => $saldo_update
        );
        
        $this->db->query("call update_trans_token_listrik('".($kode_token)."', ".(($saldo_update)-($jumlah_saldo)).")");

        $this->db->where('kode_submultipayment',$kode_token);
        $this->db->update('tbl_t_transaksi',$data_array);
        $data['kode_trans'] = null;
            $data['status'] = true;
    }
        // var_dump($this->db->affected_rows());
        // die;
    
    //     if($this->db->affected_rows() > 0)
    //     {
    //         $status_save = true; 
    //      //$this->db->trans_rollback();
    //          $this->db->trans_commit();
    //     }else{
    //      $status_save = false;
    //      $this->db->trans_rollback();
    //  }

} catch (Exception $e) {
      //$this->db->trans_rollback();
}
 return $data;//$status_save;
}


function delete($kode_token)
{
    $this->db->where('kode_token',$kode_token);
    $this->db->delete('tbl_m_token_listrik');
    return true;
}
}