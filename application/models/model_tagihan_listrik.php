<?php
class model_tagihan_listrik extends ci_model{

 public function __construct() 
 {
   parent::__construct(); 
   $this->load->database();
 }
 function get_data()
 {
  $query= "SELECT kode_tagihan,no_meter,DATE_FORMAT(tgl_transaksi,'%d-%c-%Y') as tgl_transaksi,id_konsumen,DATE_FORMAT(periode,'%d-%c-%Y') as periode,harga,kwh FROM tbl_m_tagihan_listrik ";
  return $this->db->query($query)->result();
}

function get_data_by_code($kode_tagihan)
{
    $query= "SELECT kode_tagihan,no_meter,DATE_FORMAT(tgl_transaksi,'%Y-%m-%d') as tgl_transaksi,id_konsumen,DATE_FORMAT(periode,'%Y-%m-%d') as periode,harga,kwh FROM tbl_m_tagihan_listrik WHERE kode_tagihan ='".$kode_tagihan."'";
    $data= $this->db->query($query)->row();
    
    return $data;
}

function save($data){
 $kode_Tagihan = $data[0]['kode_tagihan'];

   // var_dump($data);
   // die;
 if (\strpos($kode_Tagihan, '**') !== false) {
  $query = "SELECT (substring(max(kode_tagihan),3,3)*1) as kode FROM tbl_m_tagihan_listrik ;";
  $count_data = $this->db->query($query)->row()->kode;
  $length_data = strlen($count_data);
  $kode_tagihan_listrik = generate_code("KT",$length_data, $count_data);
  $data_array = array(
    'KODE_TAGIHAN'=>  $kode_tagihan_listrik,
    'NO_METER' => $data[0]['no_meter'],
    'ID_KONSUMEN' => $data[0]['id_konsumen'],
    'TGL_TRANSAKSI' =>date('Y-m-d',strtotime($data[0]['tgl_transaksi'] )),
    'PERIODE' => date('Y-m-d',strtotime($data[0]['periode'] )),
    'HARGA'=> $data[0]['harga'],
    'KWH' => $data[0]['kwh']
  );
  $this->db->insert('tbl_m_tagihan_listrik', $data_array);
  
  $query = "SELECT (substring(max(kode_transaksi),3,3)*1) as kode FROM tbl_t_transaksi ;";
  $count_data = $this->db->query($query)->row()->kode;
  $length_data = strlen($count_data);
  $kode_transaksi = generate_code("TR",$length_data, $count_data);
  $data_array = array(
    'KODE_TRANSAKSI'=>  $kode_transaksi,
    'KODE_MULTIPAYMENT' => 'MP003',
    'KODE_SUBMULTIPAYMENT' => $kode_tagihan_listrik,
    'TGL_TRANSAKSI' =>date('Y-m-d',strtotime($data[0]['tgl_transaksi'] )),
    'ADD' => 0,
    'RED' => $data[0]['harga'],
    'TOTAL' => get_last_saldo()-$data[0]['harga']
  );
  $this->db->insert('tbl_t_transaksi', $data_array);
  if($this->db->affected_rows() > 0)
  {
    $data['status'] = '200'; 
    $data['kode_trans'] = $kode_tagihan_listrik;
    $data['msg'] = 'sucess'; 
  }else{
    $data['status'] = '500'; 
    $data['msg'] = 'failed'; 
  }
}else{
 $data = array(
   'NO_METER' => $data[0]['no_meter'],
   'ID_KONSUMEN' => $data[0]['id_konsumen'],
   'TGL_TRANSAKSI' =>date('Y-m-d',strtotime($data[0]['tgl_transaksi'] )),
   'PERIODE' => date('Y-m-d',strtotime($data[0]['periode'] )),
   'HARGA' => $data[0]['harga'],
   'KWH' => $data[0]['kwh']
 );

 $query = "SELECT harga FROM tbl_m_tagihan_listrik WHERE kode_tagihan='".$kode_Tagihan."'";
  $jumlah_saldo =  $this->db->query($query)->row()->harga;
  $saldo_update =  $data['HARGA'];

   $this->db->where('KODE_TAGIHAN',  $kode_Tagihan);
 $this->db->update('tbl_m_tagihan_listrik', $data);

//SALDO_AKHIR + (CHANGE - ORI) = 
  $data_array = array(
    'TGL_TRANSAKSI' =>date('Y-m-d',strtotime($data['TGL_TRANSAKSI'] )),
    'RED' => $saldo_update
  );
  
  $this->db->query("call update_trans_tagihan_listrik('".($kode_Tagihan)."', ".(($saldo_update)-($jumlah_saldo)).")");

  $this->db->where('kode_submultipayment',$kode_Tagihan);
  $this->db->update('tbl_t_transaksi',$data_array);

 $data['status'] = '200';
 $data['kode_trans'] = null; 
 $data['msg'] = 'sucess';
}
return $data;
}

function update_data($data,$kode_Tagihan)
{
  $status_update =false;
  $data = array(
   'NO_METER' => $data[0]['no_meter'],
   'ID_KONSUMEN' => $data[0]['id_konsumen'],
   'TGL_TRANSAKSI' => $data[0]['tgl_transaksi'],
   'PERIODE' => $data[0]['periode'],
   'HARGA' => $data[0]['harga'],
   'KWH' => $data[0]['kwh']
 );

  $this->db->where('KODE_TAGIHAN',  $kode_Tagihan);
  $this->db->update('tbl_m_tagihan_listrik', $data);

  $status_update= true;  
  return $status_update;
}

function delete_data($kode_tagihan){
  $this->db->where('KODE_TAGIHAN', $kode_tagihan);
  $this->db->delete('tbl_m_tagihan_listrik');
  return true;
}

function get_list_toys()
{
  $query= "SELECT * 
  FROM tbl_toys 
  WHERE STATUS=1";
  $data = $this->db->query($query)->result();
  return $data;
}
function get_list_toys_by_id($id)
{
  $query= "SELECT * 
  FROM tbl_toys 
  WHERE STATUS=1 
  AND TOY_CODE NOT IN(SELECT TOY_CODE FROM tbl_package_detail WHERE PACKAGE_CODE='".$id."')";
  $data = $this->db->query($query)->result();
  return $data;
}
function get_package_by_code($package_code)
{
  $query= "SELECT * FROM tbl_package WHERE package_code='".$package_code."'";
  $data= $this->db->query($query)->row();

  return $data;
}
function get_package_detail($package_code)
{

  $query= "SELECT  tbd.TOY_CODE,tbt.TOY_DESC,tbd.STATUS
  FROM tbl_package_detail tbd
  INNER JOIN tbl_toys tbt ON tbt.TOY_CODE=tbd.TOY_CODE 
  WHERE tbd.PACKAGE_CODE='".$package_code."'";

  $data['data_toy'] = $this->db->query($query)->result();

  return $data;
}

}