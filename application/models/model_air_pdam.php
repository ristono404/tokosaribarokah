<?php
class model_air_pdam extends ci_model{

 public function __construct() 
 {
   parent::__construct(); 
   $this->load->database();
 }
 function get_data()
 {
  $query= "SELECT kode_pdam,no_pdam,DATE_FORMAT(tgl_transaksi,'%d-%c-%Y') as tgl_transaksi,tmk.nm_konsumen, DATE_FORMAT(periode,'%d-%c-%Y') as periode,harga FROM tbl_m_air_pdam tma  ,tbl_m_konsumen tmk where tma.id_konsumen = tmk.kode_konsumen";
  return $this->db->query($query)->result();
}

function get_data_by_code($kode_pdam)
{
  $query= "SELECT kode_pdam,no_pdam,DATE_FORMAT(tgl_transaksi,'%Y-%m-%d') as tgl_transaksi,id_konsumen,DATE_FORMAT(periode,'%Y-%m-%d') as periode,harga FROM tbl_m_air_pdam WHERE kode_pdam ='".$kode_pdam."'";
  $data= $this->db->query($query)->row();

  return $data;
}

function save($data){

 $kode_pdam = $data[0]['kode_pdam'];
 if (\strpos($kode_pdam, '**') !== false) {
  $query = "SELECT (substring(max(kode_pdam),3,3)*1) as kode FROM tbl_m_air_pdam ;";
  $count_data = $this->db->query($query)->row()->kode;
  $length_data = strlen($count_data);
  $kode_pdam = generate_code("KP",$length_data, $count_data);
  
  $data_array = array(
    'KODE_PDAM'=>  $kode_pdam,
    'NO_PDAM' => $data[0]['no_pdam'],
    'ID_KONSUMEN' => $data[0]['id_konsumen'],
    'TGL_TRANSAKSI' => date('Y-m-d',strtotime($data[0]['tgl_transaksi'] )) ,
    'PERIODE' => date('Y-m-d',strtotime($data[0]['periode'] )),
    'HARGA' => $data[0]['harga']
  );

  $this->db->insert('tbl_m_air_pdam', $data_array);

  $query = "SELECT (substring(max(kode_transaksi),3,3)*1) as kode FROM tbl_t_transaksi ;";
  $count_data = $this->db->query($query)->row()->kode;
  $length_data = strlen($count_data);
  $kode_transaksi = generate_code("TR",$length_data, $count_data);
  $data_array = array(
    'KODE_TRANSAKSI'=>  $kode_transaksi,
    'KODE_MULTIPAYMENT' => 'MP004',
    'KODE_SUBMULTIPAYMENT' => $kode_pdam,
    'TGL_TRANSAKSI' =>date('Y-m-d',strtotime($data[0]['tgl_transaksi'] )),
    'ADD' => 0,
    'RED' => $data[0]['harga'],
    'TOTAL' => get_last_saldo()-$data[0]['harga']
  );
  $this->db->insert('tbl_t_transaksi', $data_array);
  if($this->db->affected_rows() > 0)
  {
    $data['status'] = '200'; 
    $data['msg'] = 'sucess'; 
    $data['kode_trans'] = $kode_pdam;
  }else{
    $data['status'] = '500'; 
    $data['msg'] = 'failed'; 
  }
}else{  
  $data = array(
   'NO_PDAM' => $data[0]['no_pdam'],
   'ID_KONSUMEN' => $data[0]['id_konsumen'],
   'TGL_TRANSAKSI' =>date('Y-m-d',strtotime($data[0]['tgl_transaksi'] )),
   'PERIODE' => date('Y-m-d',strtotime($data[0]['periode'] )),
   'HARGA' => $data[0]['harga']
 );
  $query = "SELECT harga FROM tbl_m_air_pdam WHERE kode_pdam='".$kode_pdam."'";
  $jumlah_saldo =  $this->db->query($query)->row()->harga;
  $saldo_update =  $data['HARGA'];

  $this->db->where('KODE_PDAM',  $kode_pdam);
  $this->db->update('tbl_m_air_pdam', $data);

//SALDO_AKHIR + (CHANGE - ORI) = 
  $data_array = array(
    'TGL_TRANSAKSI' =>date('Y-m-d',strtotime($data['TGL_TRANSAKSI'] )),
    'RED' => $saldo_update
  );
  $this->db->query("call update_trans_air_pdam('".($kode_pdam)."', ".(($saldo_update)-($jumlah_saldo)).")");

  $this->db->where('kode_submultipayment',$kode_pdam);
  $this->db->update('tbl_t_transaksi',$data_array);

  $data['status'] = '200'; 
   $data['kode_trans'] = null;
  $data['msg'] = 'sucess'; 
}
return $data;


}

function update_data($data,$kode_pdam)
{
  $status_update =false;
  $data = array(
   'NM_PDAM' => $data[0]['nm_pdam'],
   'ID_KONSUMEN' => $data[0]['id_konsumen'],
   'TGL_TRANSAKSI' => $data[0]['tgl_transaksi'],
   'PERIODE' => $data[0]['periode'],
   'HARGA' => $data[0]['harga']
 );

  $this->db->where('KODE_PDAM',  $kode_pdam);
  $this->db->update('tbl_m_air_pdam', $data);

  $status_update= true;  
  return $status_update;
}

function delete_data($kode_pdam){
  $this->db->where('KODE_PDAM', $kode_pdam);
  $this->db->delete('tbl_m_air_pdam');
  return true;
}

}