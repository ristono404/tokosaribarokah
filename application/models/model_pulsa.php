<?php
class model_pulsa extends ci_model{

   public function __construct() 
   {
     parent::__construct(); 
     $this->load->database();
 }
 function get_data()
 {
    $query= "SELECT tbp.*,DATE_FORMAT(tbp.tgl_transaksi,'%d-%c-%Y')tgl_transaksi , tbk.nm_konsumen FROM tbl_m_pulsa tbp, tbl_m_konsumen tbk 
where tbp.id_konsumen = tbk.kode_konsumen";
    return $this->db->query($query)->result();
}
function get_data_by_code($kode_pulsa)
{
    $query= "SELECT * FROM tbl_m_pulsa WHERE kode_pulsa='".$kode_pulsa."'";
    $data= $this->db->query($query)->row();
    
    return $data;
}

function save($data)
{
    $status_save =false;
    $object = new stdClass();

    try {
        
        $kode_pulsa = $data[0]['kode_pulsa'];

       if (\strpos($kode_pulsa, '**') !== false) {
            //SAVE HEADER
        $query = "SELECT (substring(max(kode_pulsa),3,3)*1) as kode FROM tbl_m_pulsa ;";
        $count_data = $this->db->query($query)->row()->kode;
        $length_data = strlen($count_data);
        $kode_pulsa = generate_code("PL",$length_data, $count_data);

        $data_array = array(
            'kode_pulsa'=>  $kode_pulsa,
            'id_konsumen'=>$data[0]['id_konsumen'],
            'isi_pulsa' => $data[0]['isi_pulsa'],
            'tgl_transaksi' => $data[0]['tgl_transaksi'],
            'no_telepon' => $data[0]['no_telepon'],
            'harga' => $data[0]['harga']
        );
            //SAVE HEADER
        $this->db->insert('tbl_m_pulsa', $data_array);
            //SAVE DETAIL
        $query = "SELECT (substring(max(kode_transaksi),3,3)*1) as kode FROM tbl_t_transaksi ;";
        $count_data = $this->db->query($query)->row()->kode;
        $length_data = strlen($count_data);
        $kode_transaksi = generate_code("TR",$length_data, $count_data);
        $data_array = array(
            'KODE_TRANSAKSI'=>  $kode_transaksi,
            'KODE_MULTIPAYMENT' => 'MP006',
            'KODE_SUBMULTIPAYMENT' => $kode_pulsa,
            'TGL_TRANSAKSI' =>date('Y-m-d',strtotime($data[0]['tgl_transaksi'] )),
            'ADD' => 0,
            'RED' => $data[0]['isi_pulsa'],
            'TOTAL' => get_last_saldo()-$data[0]['isi_pulsa']
        );
       $this->db->insert('tbl_t_transaksi', $data_array);
       
       $object->status = true;
       $object->kode_trans = $kode_pulsa;

    }else{
        $data_array = array(
            'kode_pulsa'=>  $kode_pulsa,
            'id_konsumen' => $data[0]['id_konsumen'],
            'isi_pulsa' => $data[0]['isi_pulsa'],
            'tgl_transaksi' => $data[0]['tgl_transaksi'],
            'no_telepon' => $data[0]['no_telepon'],
            'harga' => $data[0]['harga']
        );

        $query = "SELECT isi_pulsa FROM tbl_m_pulsa WHERE kode_pulsa='".$kode_pulsa."'";


        $jumlah_saldo =  $this->db->query($query)->row()->isi_pulsa;
        $saldo_update =  $data[0]['isi_pulsa'];


        $this->db->where('kode_pulsa',$kode_pulsa);
        $this->db->update('tbl_m_pulsa',$data_array);
       //SALDO_AKHIR + (CHANGE - ORI) = 
        $data_array = array(
            'TGL_TRANSAKSI' =>date('Y-m-d',strtotime($data[0]['tgl_transaksi'] )),
            'RED' => $saldo_update
        );
        
          
            $this->db->query("call update_trans_pulsa('".($kode_pulsa)."', ".(($saldo_update)-($jumlah_saldo)).")");

            $this->db->where('kode_submultipayment',$kode_pulsa);
            $this->db->update('tbl_t_transaksi',$data_array);
            
            $object->status = true;
            $object->kode_trans= null;

        }
       
} catch (Exception $e) {
      //$this->db->trans_rollback();
    return $e;
}
return $object;//$status_save;
}


function delete($kode_pulsa)
{
    $this->db->where('kode_pulsa',$kode_pulsa);
    $this->db->delete('tbl_m_pulsa');
    return true;
}
}