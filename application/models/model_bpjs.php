<?php
class model_bpjs extends ci_model{

 public function __construct() 
 {
   parent::__construct(); 
   $this->load->database(); 
 }
 function get_data() 
 {
  $query= "SELECT tmb.*,DATE_FORMAT(tmb.periode,'%d-%c-%Y')periode,DATE_FORMAT(tmb.tgl_transaksi,'%d-%c-%Y')tgl_transaksi,tmk.nm_konsumen FROM tbl_m_bpjs tmb,tbl_m_konsumen tmk where tmb.id_konsumen = tmk.kode_konsumen";
  return $this->db->query($query)->result();
}
function get_data_by_code($kode_bpjs)
{
  $query= "SELECT * FROM tbl_m_bpjs WHERE kode_bpjs='".$kode_bpjs."'";
  $data= $this->db->query($query)->row();
  
  return $data;
}

function save($data)
{
  $status_save =false;
  $object = new stdClass();

  try {
        
   $kode_bpjs = $data[0]['kode_bpjs'];

   if (\strpos($kode_bpjs, '**') !== false) {
            //SAVE HEADER
     $query = "SELECT (substring(max(kode_bpjs),3,3)*1) as kode FROM tbl_m_bpjs ;";
     $count_data = $this->db->query($query)->row()->kode;
     $length_data = strlen($count_data);
     $kode_bpjs = generate_code("BP",$length_data, $count_data);
     $data_array = array(
      'kode_bpjs'=>  $kode_bpjs,
      'periode' => $data[0]['periode'],
      'id_konsumen' => $data[0]['id_konsumen'],
      'tgl_transaksi' => $data[0]['tgl_transaksi'],
      'no_reff' => $data[0]['no_reff'],
      'harga' => $data[0]['harga'],
    );
            //SAVE HEADER
     $this->db->insert('tbl_m_bpjs', $data_array);

     $query = "SELECT (substring(max(kode_transaksi),3,3)*1) as kode FROM tbl_t_transaksi ;";
     $count_data = $this->db->query($query)->row()->kode;
     $length_data = strlen($count_data);
     $kode_transaksi = generate_code("TR",$length_data, $count_data);
     $data_array = array(
      'KODE_TRANSAKSI'=>  $kode_transaksi,
      'KODE_MULTIPAYMENT' => 'MP005',
      'KODE_SUBMULTIPAYMENT' => $kode_bpjs,
      'TGL_TRANSAKSI' =>date('Y-m-d',strtotime($data[0]['tgl_transaksi'] )),
      'ADD' => 0,
      'RED' => $data[0]['harga'],
      'TOTAL' => get_last_saldo()-$data[0]['harga']
    );
     $this->db->insert('tbl_t_transaksi', $data_array);
       //SAVE DETAIL
      $object->status = true;
      $object->kode_bpjs = $kode_bpjs;

   }else{
    $data_array = array(
      'kode_bpjs'=>  $kode_bpjs,
      'periode' => $data[0]['periode'],
      'id_konsumen' => $data[0]['id_konsumen'],
      'tgl_transaksi' => $data[0]['tgl_transaksi'],
      'no_reff' => $data[0]['no_reff'],
      'harga' => $data[0]['harga'],
    );

    $query = "SELECT harga FROM tbl_m_bpjs WHERE kode_bpjs ='".$kode_bpjs."'";
    $jumlah_saldo =  $this->db->query($query)->row()->harga;
    $saldo_update =  $data[0]['harga'];
    
    $this->db->where('kode_bpjs',$kode_bpjs);
    $this->db->update('tbl_m_bpjs',$data_array);


    $data_array = array(
      'TGL_TRANSAKSI' =>date('Y-m-d',strtotime($data[0]['tgl_transaksi'] )),
      'RED' => $saldo_update
    );
    
    $this->db->query("call update_trans_bpjs('".($kode_bpjs)."', ".(($saldo_update)-($jumlah_saldo)).")");

    $this->db->where('kode_submultipayment',$kode_bpjs);
    $this->db->update('tbl_t_transaksi',$data_array);

    $object->status = true;
    $object->kode_bpjs = null;

  }
} catch (Exception $e) {
      //$this->db->trans_rollback();
}
 return $object;//$status_save;
}


function delete($kode_bpjs)
{
  $this->db->where('kode_bpjs',$kode_bpjs);
  $this->db->delete('tbl_m_bpjs');
  return true;
}
}