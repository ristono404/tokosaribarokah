<?php
class model_paket_data extends ci_model{

 public function __construct() 
 {
   parent::__construct(); 
   $this->load->database();
}
function get_data()
{
    $query= "SELECT tmp.*,DATE_FORMAT(tmp.tgl_transaksi,'%d-%c-%Y')tgl_transaksi,tmk.nm_konsumen FROM tbl_m_paket_data tmp, tbl_m_konsumen tmk where tmp.id_konsumen = tmk.kode_konsumen";
    return $this->db->query($query)->result();
}
function get_data_by_code($kode_paket_data)
{
    $query= "SELECT * FROM tbl_m_paket_data WHERE kode_paket_data='".$kode_paket_data."'";
    $data= $this->db->query($query)->row();
    
    return $data;
}

function save($data)
{
    // $data = json_decode($data,true);
    $status_save =false;
    
    $object = new stdClass();

    try {
        $kode_paket_data = $data[0]['kode_paket_data'];

     if (\strpos($kode_paket_data, '**') !== false) {
            //SAVE HEADER
         $query = "SELECT (substring(max(kode_paket_data),3,3)*1) as kode FROM tbl_m_paket_data ;";
         $count_data = $this->db->query($query)->row()->kode;
         $length_data = strlen($count_data);
         $kode_paket_data = generate_code("PD",$length_data, $count_data);
         $data_array = array(
            'kode_paket_data'=>  $kode_paket_data,
            'id_konsumen' => $data[0]['id_konsumen'],
            'isi_paket' => $data[0]['isi_paket'],
            'tgl_transaksi' => $data[0]['tgl_transaksi'],
            'no_telepon' => $data[0]['no_telepon'],
            'harga' => $data[0]['harga']
        );
            //SAVE HEADER
         $this->db->insert('tbl_m_paket_data', $data_array);
            //SAVE DETAIL
         $query = "SELECT (substring(max(kode_transaksi),3,3)*1) as kode FROM tbl_t_transaksi ;";
         $count_data = $this->db->query($query)->row()->kode;
         $length_data = strlen($count_data);
         $kode_transaksi = generate_code("TR",$length_data, $count_data);
         $data_array = array(
            'KODE_TRANSAKSI'=>  $kode_transaksi,
            'KODE_MULTIPAYMENT' => 'MP008',
            'KODE_SUBMULTIPAYMENT' => $kode_paket_data,
            'TGL_TRANSAKSI' =>date('Y-m-d',strtotime($data[0]['tgl_transaksi'] )),
            'ADD' => 0,
            'RED' => $data[0]['harga'],
            'TOTAL' => get_last_saldo()-$data[0]['harga']
        );
         $this->db->insert('tbl_t_transaksi', $data_array);
         
         $object->status = true;
         $object->kode_trans = $kode_paket_data;

     }else{
        $data_array = array(
            'kode_paket_data'=>  $kode_paket_data,
            'id_konsumen' => $data[0]['id_konsumen'],
            'isi_paket' => $data[0]['isi_paket'],
            'tgl_transaksi' => $data[0]['tgl_transaksi'],
            'no_telepon' => $data[0]['no_telepon'],
            'harga' => $data[0]['harga']
        );
        $query = "SELECT harga FROM tbl_m_paket_data WHERE kode_paket_data ='".$kode_paket_data."'";
        $jumlah_saldo =  $this->db->query($query)->row()->harga;
        $saldo_update =  $data[0]['harga'];

        $this->db->where('kode_paket_data',$kode_paket_data);
        $this->db->update('tbl_m_paket_data',$data_array);

        $data_array = array(
            'TGL_TRANSAKSI' =>date('Y-m-d',strtotime($data[0]['tgl_transaksi'] )),
            'RED' => $saldo_update
        );
        
        
        $this->db->query("call update_trans_paket_data('".($kode_paket_data)."', ".(($saldo_update)-($jumlah_saldo)).")");

        $this->db->where('kode_submultipayment',$kode_paket_data);
        $this->db->update('tbl_t_transaksi',$data_array);
        
        $object->status = true;
        $object->kode_trans = null;

    }
        
} catch (Exception $e) {
      //$this->db->trans_rollback();
}

 return $object;//$status_save;
}


function delete($kode_paket_data)
{
    $this->db->where('kode_paket_data',$kode_paket_data);
    $this->db->delete('tbl_m_paket_data');
    return true;
}
}