<?php
class model_history extends ci_model{

   public function __construct() 
   {
     parent::__construct(); 
     $this->load->database();
 }
 function get_data($kode_multipayment=null)
 {
    $query= "SELECT ttt.kode_transaksi
                  ,DATE_FORMAT(tgl_transaksi,'%d-%c-%Y')tgl_transaksi
                  , ttt.add
                  , ttt.red 
                  , ttt.total
                  , tmm.nama_multipayment jenis_transaksi 
            FROM `tbl_t_transaksi` ttt, tbl_m_multipayment tmm 
            WHERE ttt.kode_multipayment = tmm.kode_multipayment
                 AND ttt.kode_multipayment ".($kode_multipayment==null ? "IS NOT NULL" : " ='".$kode_multipayment."'"."")."";
    return $this->db->query($query)->result();
}
function get_data_by_code($kode_transaksi)
{
    $query= "SELECT tmm.table_name, tmm.key, ttt.kode_submultipayment,tmm.nama_multipayment 
           FROM `tbl_t_transaksi` ttt, tbl_m_multipayment tmm where ttt.kode_multipayment = tmm.kode_multipayment and ttt.kode_transaksi = '".$kode_transaksi."' ";
    $data= $this->db->query($query)->row();
    $table_name = $data->table_name;
    $key = $data->key;
    $kode_submultipayment = $data->kode_submultipayment;
    $nama_multipayment = $data->nama_multipayment;
    $query = "SELECT a.*, DATE_FORMAT(a.tgl_transaksi,'%d-%c-%Y')tgl_transaksi, b.nm_konsumen,b.kode_konsumen,'".$nama_multipayment."' as nm_multipayment, '".$kode_transaksi."' as kode_transaksi  FROM ".$table_name." a, tbl_m_konsumen b  WHERE ".$key." = '".$kode_submultipayment."' and a.id_konsumen = b.kode_konsumen";
     $data= $this->db->query($query)->row();
    
    return $data;
}

function get_data_by_date($start_date,$end_date,$kode_multipayment=null)
{   
    // $time = strtotime($start_date);
    // $start_date = date('Y-m-d',$time);
    // $time = strtotime($end_date);
    // $end_date = date('Y-m-d',$time);
    $query= "SELECT ttt.kode_transaksi,
                DATE_FORMAT(tgl_transaksi,'%d-%c-%Y')tgl_transaksi,
                ttt.add, ttt.red, 
                ttt.total,
                tmm.nama_multipayment jenis_transaksi 
            FROM `tbl_t_transaksi` ttt, tbl_m_multipayment tmm 
            where ttt.kode_multipayment = tmm.kode_multipayment 
            AND ttt.kode_multipayment ".($kode_multipayment==null ? "IS NOT NULL" : " ='".$kode_multipayment."'"."")."
            AND tgl_transaksi between STR_TO_DATE('".$start_date."', '%Y-%m-%d') 
            AND STR_TO_DATE('".$end_date."', '%Y-%m-%d') ";

$data= $this->db->query($query)->result();  
return $data;;
}

function delete($kode_pulsa)
{
    $this->db->where('kode_pulsa',$kode_pulsa);
    $this->db->delete('tbl_m_pulsa');
    return true;
}

function get_master_multipayment()
{
    $data = $this->db->query("SELECT * FROM `tbl_m_multipayment` ")->result();  
    return $data;
}

}