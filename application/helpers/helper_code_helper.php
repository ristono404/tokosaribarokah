<?php

function generate_code($code_type,$length_data,$count_data)
{
   $code_customer = "CR";
   $code_final;
    if ($length_data == 1) {
        $count_data = $count_data + 1;
        $code_final =   $code_type."00".$count_data;
    } else if ($length_data == 2) {
        $count_data = $count_data + 1;
        $code_final =   $code_type."0".$count_data;
    } else if ($length_data == 3) {
        $count_data = $count_data + 1;
        $code_final =   $code_type."".$count_data;
    }

    return $code_final;
}

?>
