<?php

function chek_session_admin()
{
  $CI= & get_instance();
  $session_admin=$CI->session->userdata('logged_in_multipayment');
  if( empty($session_admin))
  {
    redirect('Login');
  }

}

function get_last_saldo(){
  $ci =& get_instance();
  $ci->load->database();
  $saldo = 0;
  $sql = "SELECT total
  FROM tbl_t_transaksi
  ORDER BY kode_transaksi DESC
  LIMIT 0,1";
  $q = $ci->db->query($sql)->row();
  if ($q != NULL) {
   $saldo =   $q->total;
 }

 return $saldo;
}

function get_first_saldo(){
  $ci =& get_instance();
  $ci->load->database();
  $sql = "SELECT total
  FROM tbl_t_transaksi
  ORDER BY kode_transaksi ASC
  LIMIT 0,1";
  $q = $ci->db->query($sql)->row();
  return $q->total;
}

function generate_code($code_type,$length_data,$count_data)
{
 $code_customer = "CR";
 $code_final;
 $count_data = $count_data + 1;
 $length_data  = strlen($count_data);
 if ($length_data == 1) {
  $code_final =   $code_type."00".$count_data;
} else if ($length_data == 2) {
  $code_final =   $code_type."0".$count_data;
} else if ($length_data == 3) {
  $code_final =   $code_type."".$count_data;
}

return $code_final;
}

function tgl_indo($tanggals){
  // 2014-08-24
  // 24-08-2014
  $tanggal=  substr($tanggals,8,2 );
  $bulan=  substr($tanggals, 5,2);
  $tahun=  substr($tanggals, 0,4);
  
  return $tanggal." ".  bulan($bulan)." ".$tahun;
}

function bulan($bulan)
{
  switch ($bulan){
      
      case 1: return 'Januari';
          break;
      case 2: return 'Februari';
          break;
      case 3: return 'Maret';
          break;
      case 4: return 'April';
          break;
      case 5: return 'Mei';
          break;
      case 6: return 'Juni';
          break;
      case 7: return 'Juli';
          break;
      case 8: return 'Agustus';
          break;
      case 9: return 'September';
          break;
      case 1: return 'Oktober';
          break;
      case 1: return 'November';
          break;
      case 1: return 'Desember';
          break;
  }

}

?>